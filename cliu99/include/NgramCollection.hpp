/*
NgramCollection.hpp

Author: Tingting Ou JHED: tou2
        Chuqiao Liu JHED: cliu99
        
Written: Nov. 12, 2016
Last modified: Dec.4, 2016

Description: Header file including constructor and function prototypes of Class NgramCollection
*/

//Include Guard
#ifndef NGRAM_COLLECTION_HPP
#define NGRAM_COLLECTION_HPP
//Necessary C/C++ library
#include <vector>
#include <list>
#include <map>
#include <string>
#include <tuple>
#include <iostream>
#include <sstream> 
#include <algorithm>
#include <cstdlib> 
#include <ctime> 
#include <cstring> 

//Class declaration of NgramCollection to hold the entries
class NgramCollection {

public:

  /*Generate an NgramCollection object with N equal to argument num*/
  NgramCollection(unsigned num) : n(num) {}

  /*Retrieve the value for N*/
  unsigned getN() const { return n; }
  
  /*Increase count for NgramCollection representing values from begin up to end
    begin is an iterator to the first word in the Ngram,
    end is an iterator to the end of the Ngram (so (end - begin) == N)*/
  void increment(std::vector<std::string>::const_iterator begin,
		 std::vector<std::string>::const_iterator end);

  /*Return the map*/
  std::map<std::vector<std::string>, unsigned> getMap();

private:

  /*the collection of entries in this NgramCollection*/
  std::map<std::vector<std::string>, unsigned> counts;

  /*the number of items ("N" in "Ngrams")in our NgramCollection*/
  unsigned n;  
};


#endif
