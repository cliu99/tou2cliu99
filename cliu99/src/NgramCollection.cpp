/*
NgramCollection.cpp

Author: Tingting Ou JHED: tou2 

Written: Nov. 12, 2016
Last modified: Dec.4, 2016

Description: Function needed for class NgramCollection to increment Ngrams.
*/

//include header file
#include "../include/NgramCollection.hpp"

	/*Increase count for NgramCollection representing values from begin up to end
	begin is an iterator to the first word in the Ngram,
	end is an iterator to the end of the Ngram (so (end - begin) == N)*/
	void NgramCollection::increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end) {
			std::vector<std::string> Ngram;//a vector of string, namely Ngram
			//Store all the n words to the Ngram vector
			for (auto it = begin; it != end; it++) {
				Ngram.push_back(*it);	
			}
			auto find = counts.find(Ngram);//try find this Ngram in our map
			if (find != counts.end()) {//if we find the Ngram, increase the count
				counts[Ngram]++;
			} else {//if we fail to find the Ngram, create one
				counts[Ngram]=1;
			}
	}
	
	 
	std::map<std::vector<std::string>, unsigned> NgramCollection::getMap() {
		return counts;
	}
