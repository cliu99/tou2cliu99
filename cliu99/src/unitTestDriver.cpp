/* 
unitTestDriver.cpp

Tingting Ou JHED: tou2
Chuqiao Liu JHED: cliu99

Last modified: Dec. 4, 2016

Description: The Catch framework will provide a driver that calls all the tests.
*/
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../include/catch.hpp"
