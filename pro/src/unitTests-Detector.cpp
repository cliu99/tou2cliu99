#include "catch.hpp"
#include "Detector.hpp"

#include <string>
#include <sstream>
#include <vector>

using std::string;
using std::vector

TEST_CASE("DetectorRead", "[getReadList]") {
	Detector b("m");
	CHECK ( b.readList("../data/test/dne.txt").empty());
	CHECK( (b.readList("../data/test/list.txt")).at(0) == "../data/test/t1.txt");
	CHECK( (b.readList("../data/test/list.txt")).at(1) == "../data/test/t2.txt");
	CHECK( (b.readList("../data/test/list.txt")).at(2) == "../data/test/t3.txt");
	
}

TEST_CASE("Comapre", "[compareFile1AndFile2]"){
	Detector b("m");
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t2.txt") == 4);
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t3.txt") == 0);
	CHECK(b.Compare ("../data/test/t2.txt", "../data/test/t3.txt") == 0);	
}

TEST_CASE("Printpair") {
	Detector b("m");
	b.readList("list.txt");
	CHECK ( b.printPair() == "Suspicious Pairs Detected at Medium Level are as followed:\n../data/test/t1.txt AND ../data/test/t2.txt\n" );
}

TEST_CASE("GetColl"){
	Detector b("m");
	b.readList("list.txt");
	vector<string> v1;
	v1.push_back("apple");
	v1.push_back("pen");
	vector<string> v2
	v2.push_back("pineapple")
	v2.push_back("pen");
	vector<string> v3;
	v3.push_back("PenPineapple");
	v3.push_back("ApplePen");
	CHECK( b.getColl("../data/test/t1.txt").at(v1) == 1);
	CHECK ( b.getColl("../data/test/t1.txt").at(v2) == 1);
	CHECK ( b.getColl("../data/test/t1.txt").at(v3) == 1);
	CHECK( b.getColl("../data/test/t2.txt").at(v1) == 1);
	CHECK( b.getColl("../data/test/t2.txt").at(v2) == 1);
	CHECK( b.getColl("../data/test/t2.txt").at(v3) == 2);

}
