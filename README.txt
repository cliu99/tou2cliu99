600.120  Section 3 Fall 2016
==Assignment 6==
Authors:
Tingting Ou  JHED: tou2
Chuqiao Liu  JHED: cliu99
====
Last modified: 2016-10-17
====

Introduction:
This program is designed to manipulate PPM image files. It is text-based UI and does not have graphical interface. The operations include: read an image from a file; write an image to a file; crop an image; invert the colors; swap color channels; convert to grayscale; increase/decrease brightness; increase/decrease contrast; Gaussian blur; sharpen; add rainbow strip effect; and rotate the image clockwise by 90 degree.
In our program design for user inputs, we design the operation command to have certain requirements. 
First of all, to read a file and write a file, the user has to exactly input 2 parts of characters: the first is a single letter(‘r’ or ‘w’), the second (separated by space(s), is a filename). 
Second, for cropping an image, the user has to input ‘c’ and followed by exactly 4 integer numbers specifying boundaries. 
Third, for inversion and swap, we ask the user to input only ‘i’ or ’s’ (and followed only by whitespaces).
Fourth, for grayscale conversion, we ask the user to input only ‘g’ (can be followed by whitespaces).
Fifth, to adjust brightness, the user has to type ‘br’ and a signed integer separated by whitespace(s).
Sixth, to adjust contrast, the user has to type ‘cn’ and a float number separated by whitespace(s).
Seventh, to create blur, the user has to type ‘bl’ and a double to specify the radius separated by whitespace(s).
Eighth, to sharpen the image, the user has to type ‘sh’ followed by a double that specifies the intensity, and another double that specifies the radius, both being separated by whitespace(s).
Last but not least, we added two extra image processing functions: rainbow effect and rotation. The rainbow effect, as the name suggests, will divide the image into 7 strips, pixels that are in each strip will be given a single color from the rainbow (red, orange, yellow, green, blue, etc). The rotation effect will rotate the image clockwise by 90 degree. For the rainbow effect, the user has to type ’t’ and nothing else. For the rotation effect the user has to type ‘o’ and nothing else.

List of source files:
a6.c - main program; the main() function in our implementation literally calls the main menu function, then returns 0.
imageManip.c - contains implementations of all image manipulation algorithms
imagemanip.h - contains headers of all image manipulation algorithms
menuUtil.c - contains implementations of all the functions related to printing the menu, reading user input, etc.
menuUtil.h - contains headers of all the functions related to printing the menu, reading user input, etc.
ppmIO.c - contains implementations of functions for reading, writing, creating, destroying, copying, etc. images (using the PPM format)
ppmIO.h - contains headers of functions for reading, writing, creating, destroying, copying, etc. images (using the PPM format)
Makefile - this file helps generating necessary files to run the program.

** NOTE: We use if-else statement instead of switch for the main menu because we have to get 2 characters and check them.

Incremental Plan:

Phase I:
Step 1: We first assume the input is correct, the image is valid, and build the readImage function.
Step 2: We still assume the input from user is 'r' or 'w' and build the writeImage function.
Step 3: We still assume the input is correct and build the crop image function. Now we have a program that reads a PPM image, crop it, and write it out.
Step 4: We still assume the input is correct and build the inversion image function.
Step 5: We build the swap function.
Step 6: We add a part of code that checks if a PPM file is valid to the readImage function.
Step 7: We build the main function and include all the valid input checks.
Step 8: Now that we have everything in one big program working and split up them into the designated files.
Step 9: We run the program as a whole from the a6.c file and make edits to the program.
Step 10: We find extra PPM files to test the program.

Phase II:
Step 1: we first write the grayscale function and complete the mainmenu function part for grayscale.
Step 2: we then do the same for the brightness function and add a saturation function to clamp the RGB values.
Step 3: we then do the same for the contrast function.
Step 4: Since the Gaussian blur function is pretty complicated, we break up the function into 2 parts: a convolution function that calculates g, change a single RGB value, and return a normalized RGB values; a blur function that uses a template to store the blurred pixel values and replace the original image data field with the template.
Step 5: After the blur function is done, the sharpen function is pretty simple to write.
Step 6: We come up with 2 extra functions: rainbow effect and rotation. 
Step 7: We debug and add comments, edit README and get a git log file.
Step 9: tarball everything.

Thoughts and Feedback:
This is a huge assignment and we really put a lot of efforts into writing this program together. Since both of us were busy during the week, we did not start working till Friday night. We spent our entire Saturday to write this program together, from which we learned the importance of “starting early”. 
This assignment is very interesting but also has a huge workload. Nika is so cute, it cheered us up when we had seg faults.
The writeImage demo in class was also very helpful. But the pointer and malloc part of the program is still difficult.
