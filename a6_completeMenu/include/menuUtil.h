/* menuUtil.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for printing menu and reading user input functions.
 *
 */

//include guard

#ifndef MENUUTIL_H
#define MENUUTIL_H

//include necessary C library and header files

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"

//function prototypes go here


//Print Goodbye and exit program
void printGoodbye(char* buffer, Image *myIm);

//Processing image functions according to command line character
int readOrWrite(char ch, Image *myIm, char* buffer, int* length);

int cropOrContrast(char ch, Image *myIm, char* buffer);

int grayOrInvert(char ch, Image *myIm, char* buffer);

int sharpenOrSwap(char ch, Image *myIm, char* buffer);

int blurOrBrighten(char ch, Image *myIm, char* buffer);


//Function to read user input and call other PPM image processing functions
int mainmenu();

//Function to print a menu
int printmenu();

#endif 


