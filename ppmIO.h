/* ppmIO.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for reading and writing PPM image functions.
 *
 */

//include guard

#ifndef PPMIO_H
#define PPMIO_H

//Include necessary C library
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

//Struct definition for Pixel and Image here

//Pixel has three values, r=red, g=green, b=blue
typedef struct _pixel {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} Pixel;

//An image consists of a Pixel array and three parameters: the height and width of the image, and max value of colors
typedef struct _image {
	Pixel *data;
	int rows;
	int cols;
	int colors;
} Image;

/* This function reads a PPM image with a name
 * specified by the user. If the PPM file is not 
 * found, or if it is invalid, it will return an error message.
 * If the PPM file is found, it will also print out a message
 * and store the values (rows, cols, colors) read from the PPM 
 * file into the myIm struct.
 */ 
int readimage(Image* myIm, char *filename);

/* This function will write after a PPM image is read.
 * If nothing is read and the user asks for write, it 
 * will print out an error message. After putting values
 * into the myIm struct it also double checks if the 
 * assignment is successful.
 */
int writeimage(Image* myIm, char* filename);

#endif 
