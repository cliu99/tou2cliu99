/*
NgramCollection.cpp

Author: Tingting Ou JHED: tou2 

Written: Nov. 12, 2016
Last modified: Nov. 13, 2016

Description: Functions needed for class NgramCollection to increment Ngrams, get number N, display details of all Ngrams according to a specific order and pick the next word according to first (n-1) words 
*/

//include header file
#include "../include/NgramCollection.hpp"

	/*Increase count for NgramCollection representing values from begin up to end
	begin is an iterator to the first word in the Ngram,
	end is an iterator to the end of the Ngram (so (end - begin) == N)*/
	void NgramCollection::increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end) {
			std::string nextWord = *(end-1);//store the last word in Ngram
			std::vector<std::string> leadingWords;// the words to follow in Ngram
			//Store all the n-1 words to our leadingWords array
			for (auto itStringArr = begin; itStringArr!=(end-1); itStringArr++) {
				leadingWords.push_back(*itStringArr);	
			}
			if (counts[leadingWords].empty()) {//if it's the first time we collect these leading words, add a new map to store next words & frequencies
				std::map<std::string, unsigned> next_freq;
				next_freq[nextWord] = 1;
				counts[leadingWords] = next_freq;
			} else {//if it's NOT the first time we collect these leading words
				auto it = (counts[leadingWords]).find(nextWord);//try find the next word in the map
				if (it != (counts[leadingWords]).end()) {//if we find this next word in map, just add the count
					counts[leadingWords][nextWord]++;
				} else {//if we fail to find this next word in map, create an item in the map
					counts[leadingWords][nextWord]=1;
				}
			}
	}

	/*Retrieve the string representation of this NgramCollection (one entry per line) in specified order*/
	/*will call one of the below (defaults to alpha if no argument) specialized print-sorted functions*/
	std::string NgramCollection::toString(char order) const { 
		std::string result;
		if (order == 'a') {//'a' means in forwarding alphabetical order
			result = toStringAlpha();
		} else if (order == 'r') {//'r' means in reversing alphabetical order
			result = toStringReverseAlpha();
		} else {//'c' means in reversing alphabetical order
			result = toStringCount();
		}
		return result;
	}
	
	//specialized print-sorted functions below:
		
	/*Retrieve the string representation of this NgramCollection in forwarding alphabetical order*/
	std::string NgramCollection::toStringAlpha() const {
		std::stringstream ss;
		for (auto it = counts.begin(); it != counts.end(); it++) {//"it" is a map iterator to traverse the whole map to visit each key-value pair
			for (auto it3 = (it->second.begin()); it3 != (it->second.end()); it3++) {//"it3" is an iterator to traverse the inner map of a specific n-1 leading words
				for (auto it2 = (it->first.begin()); it2 != (it->first.end()); it2++) {//"it2" is an iterator to traverse the n-1 words before the next word. Need to print the n-1 words first before we print the next word and the frequency
					ss << *(it2) << " ";//print the n-1 words first
				}
				ss << it3->first << " " << it3->second << '\n';//then print the next word and also the frequency
			}
		}
		return (ss.str());	 
	}
	
	/*Retrieve the string representation of this NgramCollection in reversing alphabetical order*/
	std::string NgramCollection::toStringReverseAlpha() const {
		std::stringstream ss;
		for (auto it = counts.rbegin(); it != counts.rend(); it++) {//"it" is a map iterator to traverse the whole map to visit each key-value pair
			for (auto it3 = (it->second.rbegin()); it3 != (it->second.rend()); it3++) {//"it3" is a reverse iterator to traverse the inner map of a specific n-1 leading words
				for (auto it2 = (it->first.begin()); it2 != (it->first.end()); it2++) {//"it2" is an iterator to traverse the n-1 words before the next word. Need to print the n-1 words first before we print the next word and the frequency
					ss << *(it2) << " ";//print the n-1 words first
				}
				ss << it3->first << " " << it3->second << '\n';//then print the next word and also the frequency
			}
		}
		return (ss.str());	 
	}
	 /*Retrieve the string representation of this NgramCollection in count order; if tied, use forwarding alphabetical order to break tie*/
	std::string NgramCollection::toStringCount() const {
		std::stringstream ss;
		//CountsVec is a new data structure to hold all the values from our NgramCollection
		std::vector < std::pair< std::vector<std::string>, std::pair<std::string, unsigned> > > countsVec;
		for (auto itr = counts.begin(); itr != counts.end(); itr++) {//"itr" is a map iterator to traverse all the Ngrams in our map
			std::vector<std::string> leads;//leads is a string vector to hold the n-1 leading words. It would be used for multiple times if there are many possile words after the vector of n-1 leading words.
			for (auto itl = (itr->first).begin(); itl != (itr->first).end(); itl++) {//"itl" is a vector of string iterator to traverse the n-1 words and store all the leading words in "leads"
				leads.push_back(*itl);
			}
			for (auto itin = (itr->second).begin();itin != (itr->second).end(); itin++) {//"itin" is an iterator to traverse the inner map of a specific n-1 leading words
				std::string word = itin->first;//one possible next word
				unsigned count = itin->second;//the corresponding frequency
				std::pair<std::string, unsigned> next_freq (word, count);//construct a word-frequency pair
				std::pair< std::vector<std::string>, std::pair<std::string, unsigned> > Ngram (leads, next_freq);//Construct an Ngram which contains the N-1 words vector, and the pair of nextword-frequency
				countsVec.push_back(Ngram);//add this combination to countsVec
			}
		}
		//sort the vector using our elementCompare function
		sort(countsVec.begin(),countsVec.end(),&elementCompare);
		//Traverse our vector and print the result
		for (auto itVec = countsVec.begin(); itVec != countsVec.end(); itVec++) {//iterator to visit every Ngram
			for (auto it = (itVec->first).begin(); it != (itVec->first).end(); it++) {//iterator to first print the n-1 words in Ngram
				ss << (*it) << " ";
			}
			ss << (itVec->second).first << " " << (itVec->second).second << '\n';//then print the next word and also the frequency
		}
		return (ss.str());
	}
	

	/*Based on the probability distribution present in this NgramCollection, select 
	a word to follow the N-1 strings from begin up to end*/
	std::string NgramCollection::pickWord(std::list<std::string>::const_iterator begin, std::list<std::string>::const_iterator end) const{
		std::string nextWord;//the next word
		unsigned tot = 0;//total number of possile next words (repeats allowed)
		std::vector<std::string> leads;//the n-1 leading words
		//Add n-1 words to our variable "leads"
		for (std::list<std::string>::const_iterator itWord = begin; itWord != end; itWord++) {
			leads.push_back(*itWord);
		}
		//Sum up the total number of possible next words (repeats allowed)
		for(std::map<std::string ,unsigned>::const_iterator it = (counts.at(leads)).begin();
			it != (counts.at(leads)).end(); it++) {
			tot += it->second;
		}
		unsigned rnd = rand() % tot;//get a random number rnd
		
		//Iterate through inner map to determine which is the second word corresponding to rnd.  
		//That is the word that should follow our "leads".  
		for(std::map<std::string ,unsigned>::const_iterator it = (counts.at(leads)).begin(); it != (counts.at(leads)).end(); it++) {
			tot -= it->second;//subtract the count from total
			if (tot <= rnd) {//if total is less than our random number, and we pick this word as our next word
			  nextWord = it->first;
			  break; //end for loop since found the right word to use
			}
		 }
		return nextWord;
	}
	
	/*Compare function for sorting the NgramCollection in count order and break tie; it is static since it does not depend on any specific object of this class*/
	bool NgramCollection::elementCompare(std::pair<std::vector<std::string>, std::pair<std::string, unsigned> > p1, std::pair<std::vector<std::string>, std::pair<std::string, unsigned> > p2){
		unsigned count1 = p1.second.second;//frequency(count) of Ngram 1
		unsigned count2 = p2.second.second;//frequency(count) of Ngram 2
		//If the frequencies are different,we compare the vector of string(leading words)
		//and then the string in the pair(next word)
		if (count1 != count2) {
			return (count1 < count2);
		} else if (p1.first != p2.first) {//compare first n-1 words to break tie
			return (p1.first < p2.first);
		} else {//compare n-th word to break tie
			return (p1.second.first < p2.second.first);
		}
	
	
	}
