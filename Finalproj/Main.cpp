#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <list>
#define threshold_H 3
#define threshold_M 6
#define threshold_L 9
#define n 5
#include "NgramCollection.hpp"

using namespace std;

int main(int argc, char *argv[]) {
	switch(argc) {
		case 2:
			Detector dec(argv[1], "m");
			break; 
		case 3:
			string order = argv[2];
			if (argv[2] != "h" && argv[2] != "m" && argv[2] != "l") {
				cout << "Error: Wrong sensitivity command!\n";
				return 0;
			}
			Detector dec(argv[1], argv[2]);
			break; 
		default: 
			cout << "Error: Invalid command line arguments!\n";
			return 0;
	}
	
}

class Detector{
	public:
	Detector(string list, string com);
	vector<string> readList(string filelistname);
	double Compare(string file1, string file2);
	NgramCollection getCol(string file);
	string printPair();
		
	private:
	string filelist;
	string order;
	vector<pair<string>> simFile;
};

Detector::Detector(string list, string com) {
	filelist = list;
	order = com;
}


vector<string> Detector::readList(string filelistname){
	std::ifstream fin;
	vector<string> myList;
	fin.open(filelistname); //read the filelist which contains names of several files
	if (!fin.is_open()) {//check if we can open the file
		std::cerr << "Cannot open Filelist!\n";
		return myList;
	}
	std::string filename;
	fin >> filename;
	int open = 0;//variable to record number of files we have read
	while (!fin.eof() && !fin.fail()) {//read file names and process each until we hit EOF
		myList.push_back(filename);
		fin >> filename;
		open++;
	}
	if (open == 0){//check if the input filelist is empty
		std::cerr << "Input filelist is empty!\n";
		return myList;
	}
	fin.close();//close the file
	for (auto file1 = myList.begin(); file1!=myList.end(); file1++) {
		for (auto file2 = myList.begin(); file2!=myList.end(); file2++) {
			if (*file1 != *file2)
				Compare(*file1, *file2);
		}
	}
	return myList;
}

NgramCollection getColl(string filename) {
	NgramCollection Coll(n);
	std::ifstream fin;
	fin.open(filename); //read the file
	if (!fin.is_open()) {//check if we can find and open the file
		std::cerr << "Failed to open one of the file!\n";
		return Coll;
	}
	std::vector<std::string> wordArray;//to hold the sentence in the file
	std::string word;
	fin >> word;//read a word in the file
	while (!fin.eof() && !fin.fail()) {//store words in wordArray; read until we hit EOF
		wordArray.push_back(word);
		fin >> word;
	}
	if (wordArray.size() == 0){//check if we read nothing from the given file
		return;
	}
	/*
	//unsigned n = coll.getN();//Get the N
	//Add the start marker to our word array; start from n-1 because we always insert at the head
	for (unsigned i = n-1; i>=1; i--) {
		std::string marker = "<START_" + std::to_string(i) + ">";
		wordArray.insert(wordArray.begin(), marker);
	}
	//Add the end marker to our word array
	for (unsigned i = 1; i<=n-1; i++) {
		std::string marker = "<END_" + std::to_string(i) + ">";
		wordArray.push_back(marker);
	 }
	*/
	//Add the Ngrams to NgramCollection in our model
	for (std::vector<std::string>::const_iterator it = wordArray.begin(); it != (wordArray.end()-n+1); it++) {
		coll.increment(it, it+n);
	}
	fin.close();//close the file
}
