#ifndef TEXTFILE_H
#define TEXTFILE_H

#include <string>

#include <sstream>

class TextFile
{
public:
    TextFile(const std::string& filename="");

    bool OpenFile(const std::string& filename);

    bool HasNext()const;

    std::string GetNext();

private:

    std::stringstream text_;
};

#endif // TEXTFILE_H
