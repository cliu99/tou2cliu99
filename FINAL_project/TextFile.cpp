#include "TextFile.h"
#include <fstream>

TextFile::TextFile(const std::string& filename)
{
    if(filename!="")
        OpenFile(filename);
}

bool TextFile::OpenFile(const std::string& filename)
{
    std::ifstream fin;
    fin.open(filename);
    if(!fin.is_open())
        return false;
    std::string temp;
    while(!fin.eof())
    {
        char c=fin.get();
        if(c>='A' && c<='Z')
            c=c-'A'+'a';

        if(c>='a' && c<='z')
            temp+=c;
        else if(c=='\n')
            temp+='\n';
        else if(temp.size()>0 && temp[temp.size()-1]!=' ')
            temp+=' ';
    }
    text_.str(temp);
    return true;
}

bool TextFile::HasNext()const
{
    return !text_.eof();
}

std::string TextFile::GetNext()
{
    std::string ret;
    text_>>ret;
    return ret;
}
