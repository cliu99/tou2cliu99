#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdio>
#include "TextSimilar.h"
#include "HashString.h"

int main(int argc,char** argv)
{

    TextSimilar comp;
    std::ifstream fin;
    std::vector<std::string> name;
    std::string listfilename;
    std::string mode;
    if(argc<=1)
    {
        std::cout<<"error argument"<<std::endl;
        return 0;
    }
    else if(argc==2)
    {
        mode="m";
        listfilename=argv[1];
    }
    else if(argc==3)
    {
        mode=argv[2];
        listfilename=argv[1];
    }


    fin.open(listfilename);
    if(!fin.is_open())
    {
        std::cout<<"file not found"<<std::endl;
        return 0;
    }

    while(!fin.eof())
    {
        std::string temp;
        std::getline(fin,temp);
        if(temp.size()>0)
            name.push_back(temp);
    }

    TextSimilar comper;
    auto ans= comper.GetSimilar(name,mode[0]);
    for(const std::pair<std::size_t,std::size_t>& cur:ans)
        std::cout<<name[cur.first]<<" AND "<<name[cur.second]<<std::endl;
}
