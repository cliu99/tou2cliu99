/*
 * unitTests-NgramCollection.cpp
 *
 * Author: Tingting Ou JHED: tou2
 * 
 * Last modified: Nov.13, 2016
 *
 * Description: Unit test cases for NgramCollection class.
 *
 * While you should definitely add lots more code, try not to remove
 * or modify any of the test cases we've provided
 */

#include "../include/catch.hpp" // simple unit-testing framework
#include "../include/NgramCollection.hpp" // header declaring the functions we want to test

#include <iostream>
#include <string>
#include <vector>
#include <list>

using std::string;
using std::vector;
using std::list;
using std::map;

/* test getN, should be pretty easy
 */
TEST_CASE("getN", "[getN]") {
  NgramCollection g3(3);
  NgramCollection g4(4);
  NgramCollection g7(7);
  CHECK(g3.getN() == 3);
  CHECK(g4.getN() == 4);
  CHECK(g7.getN() == 7);
}

/* test both getMap and increment (since getMap is the easiest way
 * of checking whether increment worked). 
 */

TEST_CASE("N=3: getMap after increment", "[getMap],[increment]") {
  NgramCollection g3(3);

  vector<string> v;
  v.push_back("Four");
  v.push_back("score");
  v.push_back("and");
  v.push_back("seven");
  
  g3.increment(v.begin(), v.end()-1);
  map myMap = g3.getMap();
  vector<string> v1;
  v1.push_back("Four");
  v1.push_back("score");
  v1.push_back("and");
  CHECK(myMap.at(v1) == 1);

  g3.increment(v.begin()+1, v.end());
  vector<string> v2;
  v2.push_back("score");
  v2.push_back("and");
  v2.push_back("seven");
  myMap = g3.getMap();
  CHECK(myMap.at(v2) == 1);

  g3.increment(v.begin(), v.end()-1);
  myMap = g3.getMap();
  CHECK(myMap.at(v1) == 2);
  CHECK(myMap.at(v2) == 1);

  g3.increment(v.begin()+1, v.end());
  g3.increment(v.begin()+1, v.end());
  myMap = g3.getMap();
  CHECK(myMap.at(v1) == 2);
  CHECK(myMap.at(v2) == 3);
}

//When N=4. test the count order printing again
TEST_CASE("N=4: getMap after increment", "[getMap],[increment]") {
  NgramCollection g4(4);

  vector<string> u;
  u.push_back("I");
  u.push_back("am");
  u.push_back("an");
  u.push_back("Hopkins");
  u.push_back("student");
  
  g4.increment(u.begin(), u.end()-1);
  u1.push_back("I");
  u1.push_back("am");
  u1.push_back("an");
  u1.push_back("Hopkins");
  map myMap = g4.getMap();
  CHECK(myMap.at(u1) == 1);
  
  g4.increment(u.begin(), u.end()-1);
  myMap = g4.getMap();
  CHECK(myMap.at(u1) == 2);

  g4.increment(u.begin()+1, u.end());
  vector<string> u2;
  u2.push_back("am");
  u2.push_back("an");
  u2.push_back("Hopkins");
  u2.push_back("student");
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 1);

  g4.increment(u.begin()+1, u.end());
  g4.increment(u.begin()+1, u.end());
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 3);
  CHECK(myMap.at(u1) == 2);
}

//When N=4. test the count order printing again
TEST_CASE("N=5: getMap after increment", "[getMap],[increment]") {
  NgramCollection g5(5);

  vector<string> vec;
  vec.push_back("The");
  vec.push_back("Cat");
  vec.push_back("In");
  vec.push_back("The");
  vec.push_back("Red");
  vec.push_back("Hat");
  
  g4.increment(u.begin(), u.end()-1);
  u1.push_back("I");
  u1.push_back("am");
  u1.push_back("an");
  u1.push_back("Hopkins");
  map myMap = g4.getMap();
  CHECK(myMap.at(u1) == 1);
  
  g4.increment(u.begin(), u.end()-1);
  myMap = g4.getMap();
  CHECK(myMap.at(u1) == 2);

  g4.increment(u.begin()+1, u.end());
  vector<string> u2;
  u2.push_back("am");
  u2.push_back("an");
  u2.push_back("Hopkins");
  u2.push_back("student");
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 1);

  g4.increment(u.begin()+1, u.end());
  g4.increment(u.begin()+1, u.end());
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 3);
  CHECK(myMap.at(u1) == 2);
}
