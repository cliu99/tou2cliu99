//Last modified: Dec. 1

#include "../include/NgramCollection.hpp"
#include "../include/Detector.hpp"

using std::map;
using std::vector;
using std::pair;
using std::string;
using std::stringstream;
using std::cout;

int main(int argc, char *argv[]) {
	string filelist;
	string order;
	if (argc == 2) {
			filelist = argv[1];
			Detector dec("m");
			vector<string> vec = dec.readList(filelist);
			if (vec.empty()) 
				return 0;
			cout << dec.printPair();
	}
	else if (argc == 3) {
			filelist = argv[1];
			order = argv[2];
			if (order != "h" && order != "m" && order != "l") {
				cout << "Error: Wrong sensitivity command!\n";
				return 0;
			}
			Detector dec(order);
			vector<string> vec = dec.readList(filelist);
			if (vec.empty()) 
				return 0;
			cout << dec.printPair();
	} else {
			cout << "Error: Invalid command line arguments!\n";
	}
	return 0;
}

