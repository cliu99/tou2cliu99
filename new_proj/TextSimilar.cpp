#include "TextSimilar.h"
TextSimilar::TextSimilar()
{

}
void TextSimilar::InitData(const std::vector<std::string>& filename, char mode)
{
    dict_.clear();

    for(std::size_t i=0;i<filename.size();++i)
    {
        TextFile textFile;
        if(!textFile.OpenFile(filename[i]))
            continue;
            unsigned n;
            switch(mode)
            {
            case 'l':
                n=n_l;
                break;
            case 'm':
                n=n_m;
                break;
            case 'h':
                n=n_h;
                break;
            }
            std::vector<std::vector<std::string> > Ngrams = textFile.getNgram(n);
           
            for (std::size_t j = 0; j< Ngrams.size(); j++) {
                std::vector<std::string> ngr = Ngrams[j];
                if(dict_.count(ngr))
                {
                    dict_[ngr][i]++;
                }
                else
                {
                    std::vector<unsigned>temp(filename.size(),0);
                    temp[i]=1;
                    dict_[ngr] = std::move(temp);
                }
            }
    }

}

unsigned TextSimilar::ComputeSimilar(std::size_t x,std::size_t y)
{
    unsigned count = 0;
    for(const std::pair<std::vector<std::string>,std::vector<unsigned> > & temp : dict_)
        count += std::min(temp.second[x], temp.second[y]);
    return count;
}


std::vector<std::pair<std::size_t,std::size_t>> TextSimilar::GetSimilar(const std::vector<std::string>& filename,char mode)
{
    std::vector<std::pair<std::size_t,std::size_t>> ret;
    InitData(filename, mode);
    for(std::size_t i=0;i<filename.size();++i)
        for(std::size_t j=i+1;j<filename.size();++j)
        {
            unsigned count = ComputeSimilar(i,j);
            bool isSimilar;
            switch(mode)
            {
            case 'l':
                isSimilar=count>threshold_l;
                break;
            case 'm':
                isSimilar=count>threshold_m;
                break;
            case 'h':
                isSimilar=count>threshold_h;
                break;
            }
            if(isSimilar)
            {
                ret.push_back(std::make_pair(i,j));
            }
        }
    return ret;
}
