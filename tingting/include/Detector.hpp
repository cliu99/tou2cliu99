/* 
 * Detector.hpp
 *
 * Author: Tingting Ou JHED: tou2
 *		   Chuqiao Liu JHED: cliu99
 * 
 * Written: Nov. 30, 2016
 * Last modified: Dec. 5, 2016
 *
 * Description: header file for Detector class.
 *
 * */

//include guard
#ifndef DETECTOR_HPP
#define DETECTOR_HPP
//Necessary C/C++ library
#include "NgramCollection.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <list>
#include <algorithm>
#include <cstdlib>  
#define threshold_H 12
#define threshold_M 15
#define threshold_L 18
#define n_h 3
#define n_m 4
#define n_l 5

class Detector{
	public:
	/*************************************
	* This constructor simply takes the user command for sensitivity and store 
	* that command in the variable called order so that other functions can have
	* easy access to the command.
	**************************************/	
	Detector(std::string command); 
	
	/**************************************
	* This function takes two files from the file list and
	* construct a Ngram map for the two text files.
	* It then reads through the two maps and looks for Ngrams that 
	* occur in both files (but regardless of how many times it occur in
	* either file). If it finds a Ngram that coincidizes in both files,
	* the function increments the variable count. After all comparison is 
	* done, the function compares count with the threshold which is determined
	* by the command for sensitivity level. If count reaches threshold, the two
	* files compared in the function will be put into a pair called simPair.
	* simpair is stored in a vector pair called simFile, a private variable
	* of this class. Finally, the function returns the variable count.
	*****************************************/
	int Compare(std::string file1, std::string file2);
	
	/**************************************
	* This function return a stringstream that will show the user
	* the suspicious pairs of files found in the dataset.
	*****************************************/
	std::string printPair();
	
	/**************************************
	* This function return a string that will show the user
	* two types of error messages: fail to open file, or the file
	* contains too few words.
	*****************************************/
	std::string printErrMsg();

	/****************************************
	* This function reads from a filelist instructed by the user
	* and if the filelist cannot be found, an error message will
	* print out and an empty vector string will be returned.
	* Then we traverse through our own list 
	* and compare each file with every other file in the list.
	* The function returns the refined filelist for the sake of Unit-Testing.
	******************************************/
	std::vector<std::string> readList(std::string filelistname);

	/**************************************************
	* This function returns a collection of Ngrams.
	* It first calls the NgramCollection class to construct a new
	* Ngramcollection whose Ngram size depends on the sensitivity level.
	* The function reads from a single file from the myList and get 
	* (3,4, or 5) consecutive words into Ngrams (called wordArray).
	* The Ngrams then are stored in the Ngramcollection.
	****************************************************/
	NgramCollection getColl(std::string file);

		
	private:
	std::string order;//"l","m" or "h"
	std::vector<std::pair<std::string, std::string>> simFile;//store all the similar file pairs we have detected
	std::map<std::string, int> errMsg;//store all the error messages
};


#endif
