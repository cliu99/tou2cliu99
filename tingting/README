600.120  Section 3 Fall 2016
==Final Project==
Authors:
Tingting Ou  JHED: tou2
Chuqiao Liu  JHED: cliu99
====
Last modified: 2016-12-5
==== 
Running: 
To run the program, use the Makefile in /proj/src/ to compile the source files. 
The executable binary is in /proj/bin/ directory.
When in /proj/src/ directory: 
Type "make" to make the program binary & all tests.
Type "make proj" to only make the program.
Type "make test" to only make the unit tests.
~~~~~~~~~~
Run the program in /proj/bin/ directory.
--Please use command line argument with the binary file (proj)! 
Use the name of the file list as the second argument, 
and a single lowercase letter "l", "m", or "h" as the third argument. 
"l" "m" and "h" stands for the low, medium and high sensitivity respectively for the program.
--If you miss the third argument, the level of sensitivity will default to medium, "m".
--The command line should be formatted exactly as stated above. If the filelist is not found or is empty, a standard error message would inform the user. If the flag is not lowercase "l", "m", or "h", another different message would inform the user and the plagiarism test will not run.
--We have provided two file lists in the /proj/data/ directory. This contains data provided by the instructor. 
You can try the following commands in /proj/bin/ directory:
./proj ../data/sm_doc_list.txt m
./proj ../data/sm_doc_list.txt l
./proj ../data/sm_doc_list.txt h
./proj ../data/med_doc_list.txt m
./proj ../data/med_doc_list.txt l
./proj ../data/med_doc_list.txt h
You can also try on other file lists.

Testing: To run tests, we have written 2 unit-tests for NgramCollection and Detector class, respectively. You can find the unit test source files in /proj/src/. 
The tests cover all the functions in these two classes.
To run the unit test, the user need to: 
1) First use Makefile in /proj/src/ to make test. Type "make test" in command line.
2) Then go to /proj/bin/ to run the binary. Type "./test1" and "./test2" to run tests.
Test1 is for NgramCollection class. Test2 is for Detector class. 

Plan of Attack: 
Our plan of attack was to first build a program that can take strings of text files and build a map whose key is a vector of strings (i.e. Ngram) and whose value is the number of occurrences of the keys in each text file. 
~ The program is case-insensitive, which means that lower case and upper case letters are treated as the same. 
~ Also, our program does not take into account the punctuations or numbers. 
(Which means, only ENGLISH LETTERS matter!)
Then we would compare each vector of strings (e.g. Ngram) in two files listed in the text file list. 
If the vector of string (i.e. Ngram) appears in both files, that would count as one incidence of plagiarism. (Since two authors may use some simple phrases in common repetitively (e.g. I think that, He argues that), it is better to not take into account the repeat uses of same Ngram in one article. Still, we record the occurrences in case that future programmers may want to take that into account!)
If the number of plagiarism incidence exceeds a threshold, for example, at medium level of sensitivity, if there are more than 15 incidences of identical quadragrams found in both files, then the files would be identified as a pair of "suspicious files" and will be reported to the user. Other thresholds have been indicated in “flag” part. Each file would be compared with all the other files in the list only once to increase efficiency. 

Design: We use object-oriented design for this project. 
First we build a class named "NgramCollection" to record all the Ngrams in a text file. It has two private variables, an unsigned int (the size of the vector of string) and a map (key: Ngram, value: number of occurences of this Ngram). It has functions to return a map which records all the Ngrams and to add Ngrams into the map. A text file will have a NgramCollection object to collect all the Ngrams in the text.
Second we build a class named "Detector" to compare text files in pair and report suspicious pairs of documents. It has three private variables, a string (the order, can only be “l”, “h”or “m”), a vector of pair of strings (to store all the suspicious pairs of documents), and a map (store some special types of error messages to avoid reporting the same messages for multiple times since we may read a file for multiple times). It has functions to read a file list and get all the file names, to compare two file and get the number of occurrences of plagiarism, to get a NgramCollection based on a single file, to print all the pairs of similar (suspicious) documents as well as error messages. We used an instance of Detector class to read data (get NgramCollection), process data (Compare two files) and finally print out the result.
[Source files]
1. main.cpp ***main function of the program, do some error checking & call functions
2. Detector.cpp ***Detector class to read & compare files in pairs, also print results & error messages(if any)
3. NgramCollection.cpp ***NgramCollection class to collect all Ngrams in a text file
4. Detector.hpp ***Header file for Detector class
5. NgramCollection.hpp ***Header file for NgramCollection class
6. unitTestDriver.cpp ***unit test driver file
7. unitTests-NgramCollection.cpp ***unit test cases for NgramCollection class
8. unitTests-Detector.cpp ***unit test cases for Detector class
9. catch.hpp ***catch framework for unit tests to work

Flags: In our program, we use a map to store Ngram as key and the number of incidences of this Ngram as value. For different sensitivity levels, we have different sizes of Ngrams. 
At low sensitivity, we record fivegrams. 
At medium sensitivity, we record fourgrams. 
At high sensitivity, we record trigrams. 
On top of that, we have different thresholds for different sensitivity levels. 
The threshold is a number of same Ngram apperances in two files above which those two text files would be regarded as "susceptible pairs for plagarism". 
At low sensitivity, the threshold is 18. 
At medium sensitivity it is 15. 
At high sensitivity it is 12. 
We made these choices of numbers for thresholds and Ngram sizes because we think low sensitivity means the files need to have larger trunks of identical texts to be identified. And as the sensitivity levels grow higher, smaller trunks of identical texts can be seen as possible incidence of plagiarism. 5 consecutive same words are very suspicious (low sensitivity), but 3 are actually not that suspicious (high sensitivity).
+++In summary,
Low - must have 18 or more same fivegrams
Medium - must have 15 or more same fourgrams
High - must have 12 or more same trigram
SPECIAL NOTICE: At low sensitivity, there should be at least 5 words in the file (since we collect fivegrams!). If not, we will print out error messages and continue to check other files. At medium sensitivity at least 4 words. At high sensitivity at least 3 words.
We store the error messages in a map and print this out after we process all files, to avoid printing these messages repetitively since we read the same file for multiple times.

Timing: For each dataset, our program runs roughly the same amount of time at all sensitivity levels. For sm_doc_list, the runing time is around 2s. For med_doc_list, the running time is around 20s. For big_doc_list, the running time is roughly 90 min. We have not tried on the large and huge document sets because it takes even longer time to run. It seems that we should first think of a more efficient algorithm and then try on those data sets.

Challenges: There are three major challenges we faced during the course of our plagiarism detector program development. The first problem is we have to decide what our standards are for each sensitivity levels. Through research and discussion, we learned that if a file contains above 12 consecutive words identical to another file, that is regarded as plagiarism in most academic institutions. So we set our thresholds of high sensitivity based on this standard (if two documents have exactly 12 consecutive words, 10 trigrams will be the same. However, we have to take into account some common used trigrams, so we set the threshold to 12 to avoid over-counting). Then we discuss and decide on thresholds for the medium and high level of sensitivity. 
The second challenge we face was to develop an algorithm that would work properly. Again through research we learned a lot of different algorithms such as fingerprinting with hash functions, the levenshtein distance algorithm, and string matching. But we felt that all these algorithms did not really correspond to our standards of plagiarsim and so we made our own algorithm. We decide to count incidences of Ngrams in paired text files and when the number of incidences of Ngram coincidence reach a certain threshold we would call it plagiarism. 
Last but not least, after writing our program, we found our code not efficient enough. For medium data sets provided to us, it takes roughly 18 seconds for our program to run. The time increases greatly as the data sets increases sizes. This would eventually become a huge problem and we tried different approaches to fix it. At first we thought the reading of textfile method (we read two files and make a map from the text file list) is repetitive and we tried to read all the text files and write all the Ngrams in the files in a big map as the keys, and their incidences in each file as values. But to our frustration, with this huge data field our program only slowed more. Also we thought the way we made our NgramCollection was kind of inefficient since we used a double loop, so we tried to fix that and managed to use only single loop, but this does not increase the speed significantly. 

Future: The first thing we think we have to improve is time-efficiency. Our program had a relatively long runtime if we try on bigger data set and that should be improved. Also, if we have more time, we would like to add a method/class that compares words so as to find plagiarism in paraphrased/translated text files. In our current case-insensitive program, we used Ngrams and the strings had to be exactly the same. We did find some useful algorithm through research concerning paraphrasing problems (such as the levenshtein method) but we wish we had more time to implement that into our program. 
