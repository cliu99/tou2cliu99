/*
* main.cpp
* 
* Author: Tingting Ou JHED: tou2
* 	  	  Chuqiao Liu JHED: cliu99
*
* Last modified: Dec.5, 2016
*
* Description: main function to detect plagiarism among texts.
*/

#include "../include/NgramCollection.hpp"
#include "../include/Detector.hpp"

using std::map;
using std::vector;
using std::pair;
using std::string;
using std::stringstream;
using std::cout;

int main(int argc, char *argv[]) {
	string filelist; //list of files to be compared.
	string order; //sensitivity level.
	if (argc == 2) { //if the user does not specify a sensitivity level, default to medium.
			filelist = argv[1];
			Detector dec("m");
			vector<string> vec = dec.readList(filelist); //call the function in Detector.cpp to read file list.
			if (vec.empty()){ //if the filelist cannot be opened or is empty
				return 0;
			}
			cout << dec.printErrMsg();//print out error messages: fail to open file OR too few words in the file
			cout << dec.printPair(); //print out suspicious pairs of files.
	}
	else if (argc == 3) {
			filelist = argv[1];
			order = argv[2];
			if (order != "h" && order != "m" && order != "l") { //if the user is malicious and enters something else.
				cout << "Error: Wrong sensitivity command! Please choose from: 'l', 'm', and 'h'. \n";
				return 0;
			}
			Detector dec(order);
			vector<string> vec = dec.readList(filelist);
			if (vec.empty()) //if the filelist cannot be opened or is empty
				return 0;
			cout << dec.printErrMsg();//print out error messages: fail to open file OR too few words in the file
			cout << dec.printPair(); //print out suspicious pairs of files.	
	}
	else { //if the user is very malicious and did not enter enough valid arguments.
			cout << "Error: Invalid command line arguments!\n";
	}
	return 0;
}

