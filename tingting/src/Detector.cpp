/* Detector.cpp
 *
 * Author: Tingting Ou JHED: tou2
 *		   Chuqiao Liu JHED: cliu99
 *
 * Last modified: Dec.5, 2016
 *
 * Description: Detector class methods to read and compare files in pair, and also print results or error messages.
 *
 * */


#include "../include/Detector.hpp"

using std::map;
using std::vector;
using std::pair;
using std::string;
using std::stringstream;
using std::cout;

/*************************************
* This constructor simply takes the user command for sensitivity and store 
* that command in the variable called order so that other functions can have
* easy access to the command.
**************************************/
Detector::Detector(string command) {
	order = command;
}

/**************************************
* This function takes two files from the file list and
* construct a Ngram map for the two text files.
* It then reads through the two maps and looks for Ngrams that 
* occur in both files (but regardless of how many times it occur in
* either file). If it finds a Ngram that coincidizes in both files,
* the function increments the variable count. After all comparison is 
* done, the function compares count with the threshold which is determined
* by the command for sensitivity level. If count reaches threshold, the two
* files compared in the function will be put into a pair called simPair.
* simpair is stored in a vector pair called simFile, a private variable
* of this class. Finally, the function returns the variable count.
*****************************************/
int Detector::Compare(string file1, string file2){
	NgramCollection c1 = getColl(file1);	
	NgramCollection c2 = getColl(file2);	
	auto map1 = c1.getMap();
	auto map2 = c2.getMap();
	int count = 0;
	for (auto it1 = map1.begin(); it1 != map1.end(); it1++) {
	    if (map2.find(it1->first)!=map2.end()) {
			count++;;	
		}
	}
	if (order == "l") {
		if (count >= threshold_L) {
			pair<string, string> simPair (file1, file2);
			simFile.push_back (simPair);
		}
	} else if (order == "m") {
		if (count >= threshold_M) {
				pair<string, string> simPair (file1, file2);
				simFile.push_back (simPair);
		}
	} else {
		if (count >= threshold_H) {
					pair<string, string> simPair (file1, file2);
					simFile.push_back (simPair);
		}
	}
	return count;
}

/**************************************
* This function return a string that will show the user
* the suspicious pairs of files found in the dataset.
*****************************************/
string Detector::printPair(){
	stringstream ss;
	string level;

	//Since the stringstream will also inform the user which 
	//sensitivity level the program used, this is just a pre-setup
	//for the general output.
	if (order == "m")
		level = "Medium";
	if (order == "l")
		level = "Low";
	if (order == "h")
		level = "High";
	ss << "Suspicious Pairs Detected at " << level << " Level are as followed:\n";
	
	//Go through the vector simFile and get the paired files.
	for (auto it = simFile.begin();it != simFile.end(); it++) {
		ss << it->first << " AND " << it->second << '\n';
	}
	return ss.str();
}

/**************************************
* This function return a string, namely the error messages:
* fail to open file or file contains too few words
* this function is to make sure that we only print out
* these messages once, not multiple times.
*****************************************/
string Detector::printErrMsg(){
	stringstream ss;
	for (auto it = errMsg.begin();it != errMsg.end(); it++) {//loop through the map and print out error messages
		ss << it->first << '\n';
	}
	return ss.str();
}

/****************************************
* This function reads from a filelist instructed by the user
* and if the filelist cannot be found, an error message will
* print out and an empty vector string will be returned.
* Then we traverse through our own list 
* and compare each file with every other file in the list.
* The function returns the refined filelist for the sake of Unit-Testing.
******************************************/
vector<string> Detector::readList(string filelistname){
	std::ifstream fin;
	vector<string> myList;
	fin.open(filelistname); //read the filelist which contains names of several files
	if (!fin.is_open()) {//check if we can open the file
		std::cerr << "Cannot open Filelist!\n";
		return myList;
	}
	std::string filename;
	fin >> filename;
	int open = 0;//variable to record number of files we have read
	while (!fin.eof() && !fin.fail()) {//read file names and process each until we hit EOF
		myList.push_back(filename);
		fin >> filename;
		open++;
	}
	if (open == 0){//check if the input filelist is empty
		std::cerr << "Input filelist is empty!\n";
		return myList;
	}
	fin.close();//close the file
	for (auto file1 = myList.begin(); file1!=myList.end(); file1++) {
		for (auto file2 = (file1+1); file2!=myList.end(); file2++) {
				Compare(*file1, *file2);
		}
	}
	return myList;
}

/**************************************************
* This function returns a collection of Ngrams.
* It first calls the NgramCollection class to construct a new
* Ngramcollection whose Ngram size depends on the sensitivity level.
* The function reads from a single file from the myList and get 
* (3,4, or 5) consecutive words into Ngrams (called wordArray).
* The Ngrams then are stored in the Ngramcollection.
****************************************************/
NgramCollection Detector::getColl(string filename) {
	int Nvalue;
	if (order == "m") {
		Nvalue = n_m;
	} else if (order == "l") {
		Nvalue = n_l;
	} else 
		Nvalue = n_h; 
	NgramCollection Coll(Nvalue);
	std::ifstream fin;
	fin.open(filename); //read the file

	std::stringstream myText;//to hold the processed texts from this file
	std::string temp;
	if (!fin.is_open()) {//check if we can find and open the file
		std::string msg = "Failed to open this file: " + filename;
		errMsg[msg] = 1; //Add this error message to our map to make sure it only appears once
		return Coll;
	}
	while (!fin.eof()){
		char c = fin.get();
		if(c>='A' && c<='Z')//convert to lower case
            c=c-'A'+'a';
        if(c>='a' && c<='z')
            temp+=c;
        else if(isspace(c))
			temp+=c;
    }
    myText.str(temp);
	
	std::vector<std::string> wordArray;//to hold the sentence in the file
	std::string word;
	myText >> word;//read a word in the stringstream
	while (!myText.eof() && !myText.fail()) {//store words in wordArray; read until we hit EOF
		wordArray.push_back(word);
		myText >> word;
	}
	if ((int)wordArray.size() < Nvalue){//check if we read too few words from this file
		std::string msg = "File contains too few words (not examined): " + filename;
		errMsg[msg] = 1; //Add this error message to our map to make sure it only appears once
		return Coll;
	}
		for (std::vector<std::string>::const_iterator it = wordArray.begin(); it != (wordArray.end()-Nvalue+1); it++) {
		Coll.increment(it, it+Nvalue);
	}
	fin.close();//close the file
	return Coll;
}
