/*
 * unitTests-NgramCollection.cpp
 *
 * Author: Tingting Ou JHED: tou2 
 *         Chuqiao Liu JHED: cliu99
 *
 * Last modified: Dec.4, 2016
 *
 * Description: Unit test cases for NgramCollection class.
 *
 */

#include "../include/catch.hpp" // simple unit-testing framework
#include "../include/NgramCollection.hpp" // header declaring the functions we want to test
/*Include Necessary Library*/
#include <iostream>
#include <string>
#include <vector>
#include <list>
/*Using statements*/
using std::string;
using std::vector;
using std::list;
using std::map;

/* test getN*/
TEST_CASE("getN", "[getN]") {
  NgramCollection g3(3);
  NgramCollection g4(4);
  NgramCollection g7(7);
  CHECK(g3.getN() == 3);
  CHECK(g4.getN() == 4);
  CHECK(g7.getN() == 7);
}

/* test both getMap and increment; 
check the content in the map to ensure that the function works correctly*/

//Test increment and getMap function when N=3
TEST_CASE("N=3: getMap after increment", "[getMap],[increment]") {
  NgramCollection g3(3);
  vector<string> v;
  v.push_back("Four");
  v.push_back("score");
  v.push_back("and");
  v.push_back("seven");
  
  g3.increment(v.begin(), v.end()-1);//Add "Four" "score" "and"
  map<vector<string>, unsigned> myMap = g3.getMap();
  vector<string> v1;
  v1.push_back("Four");
  v1.push_back("score");
  v1.push_back("and");
  CHECK(myMap.at(v1) == 1);//Check if we have "Four" "score" "and" in the map

  g3.increment(v.begin()+1, v.end());//Add "score" "and" "seven"
  vector<string> v2;
  v2.push_back("score");
  v2.push_back("and");
  v2.push_back("seven");
  myMap = g3.getMap();
  CHECK(myMap.at(v2) == 1);//Check if we have "score" "and" "seven" in the map

  g3.increment(v.begin(), v.end()-1);//Add "Four" "score" "and" again
  myMap = g3.getMap();
  CHECK(myMap.at(v1) == 2);//Check if we have two "Four" "score" "and" in the map
  CHECK(myMap.at(v2) == 1);//Check if we have the "score" "and" "seven" in the map

  g3.increment(v.begin()+1, v.end());//Add "score" "and" "seven" again 
  g3.increment(v.begin()+1, v.end());//Add "score" "and" "seven" the third time
  myMap = g3.getMap();
  CHECK(myMap.at(v1) == 2);//Check if we have two "Four" "score" "and" in the map
  CHECK(myMap.at(v2) == 3);//Check if we have three "score" "and" "seven" in the map
}

//Test increment and getMap function when N=4
TEST_CASE("N=4: getMap after increment", "[getMap],[increment]") {
  NgramCollection g4(4);

  vector<string> u;//initialize the data
  u.push_back("I");
  u.push_back("am");
  u.push_back("an");
  u.push_back("Hopkins");
  u.push_back("student");
  
  g4.increment(u.begin(), u.end()-1);//Add the first 4-gram
  vector<string> u1;
  u1.push_back("I");
  u1.push_back("am");
  u1.push_back("an");
  u1.push_back("Hopkins");
  map<vector<string>, unsigned> myMap = g4.getMap();
  CHECK(myMap.at(u1) == 1);//check if the 4-gram has been put into the map
  
  g4.increment(u.begin(), u.end()-1);//Add the first 4-gram again
  myMap = g4.getMap();
  CHECK(myMap.at(u1) == 2);//check if we have two 4-grams in the map

  g4.increment(u.begin()+1, u.end());//Add the second 4-gram
  vector<string> u2;
  u2.push_back("am");
  u2.push_back("an");
  u2.push_back("Hopkins");
  u2.push_back("student");
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 1);//check if we have the 4-gram added into the map

  g4.increment(u.begin()+1, u.end());
  g4.increment(u.begin()+1, u.end());//Add the second 4-gram twice here
  myMap = g4.getMap();
  CHECK(myMap.at(u2) == 3);
  CHECK(myMap.at(u1) == 2);//check if we have the content in the map corresponds to the data we put into
}

//Test increment and getMap function when N=5
TEST_CASE("N=5: getMap after increment", "[getMap],[increment]") {
  NgramCollection g5(5);

  vector<string> vec;
  vec.push_back("See");
  vec.push_back("The");
  vec.push_back("Cat");
  vec.push_back("In");
  vec.push_back("The");
  vec.push_back("Red");
  vec.push_back("Hat");

  g5.increment(vec.begin(), vec.end()-2);//Add the first 5-gram
  vector<string> v1;
  v1.push_back("See");
  v1.push_back("The");
  v1.push_back("Cat");
  v1.push_back("In");
  v1.push_back("The");
  map<vector<string>, unsigned> myMap = g5.getMap();
  CHECK(myMap.at(v1) == 1);//Test if we have put the 5-gram in the map
  
  g5.increment(vec.begin(), vec.end()-2);//Add the first 5-gram again
  myMap = g5.getMap();
  CHECK(myMap.at(v1) == 2);//Test if we have the two 5-grams in the map

  g5.increment(vec.begin()+1, vec.end()-1);//Add the second 5-gram in the map
  vector<string> v2;
  v2.push_back("The");
  v2.push_back("Cat");
  v2.push_back("In");
  v2.push_back("The");
  v2.push_back("Red");
  myMap = g5.getMap();
  CHECK(myMap.at(v2) == 1);//Test if we have the second 5-gra in the map
  CHECK(myMap.at(v1) == 2);//Test if we have the two first 5-grams unchanged in the map
  
  g5.increment(vec.begin()+2, vec.end());//Add the third 5-gram in the map
  vector<string> v3;
  v3.push_back("Cat");
  v3.push_back("In");
  v3.push_back("The");
  v3.push_back("Red");
  v3.push_back("Hat");
  myMap = g5.getMap();
  CHECK(myMap.at(v2) == 1);
  CHECK(myMap.at(v1) == 2);
  CHECK(myMap.at(v3) == 1);//Test if the content in the map is correct
  
}
