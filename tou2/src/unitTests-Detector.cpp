#include "../include/catch.hpp"
#include "../include/Detector.hpp"
#include "../include/NgramCollection.hpp"

#include <string>
#include <sstream>
#include <vector>
#include <iostream>

using std::string;
using std::vector;
using std::map;

//Test read list function when the filelist is empty
TEST_CASE("Read Empty list", "[getReadList]") {
	Detector b("m");
	CHECK ( b.readList("../data/test/dne.txt").empty());
	Detector c("l");
	CHECK ( c.readList("../data/test/dne.txt").empty());
	Detector d("h");
	CHECK ( c.readList("../data/test/dne.txt").empty());
}

//Test read list function using 3 different levels of sensitivity
TEST_CASE("Read file list","[getReadList]") {
	Detector b("m");
	vector<string> v1 = b.readList("../data/test/list.txt");
	CHECK( v1.at(0) == "../data/test/t1.txt");
	CHECK( v1.at(1) == "../data/test/t2.txt");
	CHECK( v1.at(2) == "../data/test/t3.txt");
	
	Detector c("l");
	v1 = c.readList("../data/test/list.txt");
	CHECK( v1.at(0) == "../data/test/t1.txt");
	CHECK( v1.at(1) == "../data/test/t2.txt");
	CHECK( v1.at(2) == "../data/test/t3.txt");
	
	Detector d("h");
	v1 = d.readList("../data/test/list.txt");
	CHECK( v1.at(0) == "../data/test/t1.txt");
	CHECK( v1.at(1) == "../data/test/t2.txt");
	CHECK( v1.at(2) == "../data/test/t3.txt");

//Another test case reading another file list

	Detector b2("m");
	vector<string> u1 = b2.readList("../data/test/pair.txt");
	CHECK( u1.at(0) == "../data/test/t4.txt");
	CHECK( u1.at(1) == "../data/test/t5.txt");
	
	Detector c2("l");
	u1 = c2.readList("../data/test/pair.txt");
	CHECK( u1.at(0) == "../data/test/t4.txt");
	CHECK( u1.at(1) == "../data/test/t5.txt");
	
	Detector d2("h");
	u1 = d2.readList("../data/test/pair.txt");
	CHECK( u1.at(0) == "../data/test/t4.txt");
	CHECK( u1.at(1) == "../data/test/t5.txt");
}

//Test Compare: Compare 2 files using medium level of sensitivity
TEST_CASE("Medium level: Compare two files", "[Compare]"){
	Detector b("m");
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t2.txt") == 18);
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t3.txt") == 1);
	CHECK(b.Compare ("../data/test/t2.txt", "../data/test/t3.txt") == 1);	

	CHECK(b.Compare("../data/test/t4.txt", "../data/test/t5.txt") == 3);

}

//Test Compare: Compare 2 files using low level of sensitivity
TEST_CASE("Low level: Compare two files", "[Compare]"){
	Detector b("l");
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t2.txt") == 17);
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t3.txt") == 0);
	CHECK(b.Compare ("../data/test/t2.txt", "../data/test/t3.txt") == 0);	
	
	CHECK(b.Compare ("../data/test/t4.txt", "../data/test/t5.txt") == 2);	
}

//Test Compare: Compare 2 files using high level of sensitivity
TEST_CASE("High level: Compare two files", "[Compare]"){
	Detector b("h");
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t2.txt") == 19);
	CHECK(b.Compare("../data/test/t1.txt", "../data/test/t3.txt") == 2);
	CHECK(b.Compare ("../data/test/t2.txt", "../data/test/t3.txt") == 2);	

	CHECK(b.Compare ("../data/test/t4.txt", "../data/test/t5.txt") == 4);	
}

//Test printPair function when there are no suspicious pairs of documents
TEST_CASE("Print suspicious pair when there are no suspicious pairs", "printPair") {
	Detector b("m");
	b.readList("../data/test/pair.txt");
	CHECK(b.printPair() == "Suspicious Pairs Detected at Medium Level are as followed:\n");
	Detector c("l");
	c.readList("../data/test/pair.txt");
	CHECK(c.printPair() == "Suspicious Pairs Detected at Low Level are as followed:\n");
	Detector d("h");
	d.readList("../data/test/pair.txt");
	CHECK(d.printPair() == "Suspicious Pairs Detected at High Level are as followed:\n");
}

//Test printPair function at Medium Level 
TEST_CASE("Medium level: Print suspicious pair", "[printPair]") {
	Detector b("m");
	b.readList("../data/test/list.txt");
	CHECK ( b.printPair() == "Suspicious Pairs Detected at Medium Level are as followed:\n../data/test/t1.txt AND ../data/test/t2.txt\n" );
}

//Test printPair function at Low Level 
TEST_CASE("Low level: Print suspicious pair", "[printPair]") {
	Detector b("l");
	b.readList("../data/test/list.txt");
	CHECK ( b.printPair() == "Suspicious Pairs Detected at Low Level are as followed:\n" );
}


//Test printPair function at High Level 
TEST_CASE("High level: Print suspicious pair", "[printPair]") {
	Detector b("h");
	b.readList("../data/test/list.txt");
	CHECK ( b.printPair() == "Suspicious Pairs Detected at High Level are as followed:\n../data/test/t1.txt AND ../data/test/t2.txt\n" );
}

//Test getNgramCollection function when the file is empty
TEST_CASE("get NgramCollection from an empty file","[getColl]"){
	Detector b("m");
	map<vector<string>, unsigned> myMap = (b.getColl("../data/test/dne.txt")).getMap();
	CHECK(myMap.empty());
	Detector c("l");
	myMap = (b.getColl("../data/test/dne.txt")).getMap();
	CHECK(myMap.empty());
	Detector d("h");
	myMap = (b.getColl("../data/test/dne.txt")).getMap();
	CHECK(myMap.empty());
}

//Test getColl function; read from a file first and use getMap to test if the data in the map is correct
TEST_CASE("3 levels: get NgramCollection from a file","[getColl]"){
	//Medium level, read t4.txt
	Detector b("m");
	map<vector<string>, unsigned> myMap = (b.getColl("../data/test/t4.txt")).getMap();
	vector<string> v1;
	v1.push_back("you");
	v1.push_back("can't");
	v1.push_back("blame");
	v1.push_back("gravity");
	vector<string> u1;
	u1.push_back("can't");
	u1.push_back("blame");
	u1.push_back("gravity");
	u1.push_back("for");
	CHECK( myMap[u1] == 1);
	CHECK( myMap[v1] == 1);
	//Low level, read t5.txt
	Detector c("l");
	myMap = (c.getColl("../data/test/t5.txt")).getMap();
	vector<string> v2;
	v2.push_back("can't");
	v2.push_back("blame");
	v2.push_back("gravity");
	v2.push_back("for");
	v2.push_back("falling");
	vector<string> u2;
	u2.push_back("you");
	u2.push_back("can't");
	u2.push_back("blame");
	u2.push_back("gravity");
	u2.push_back("for");
	CHECK( myMap[u2] == 1);
	CHECK( myMap[v2] == 1);
	//High level, read t6.txt
	Detector d("h");
	myMap = (d.getColl("../data/test/t6.txt")).getMap();
	vector<string> v3;
	v3.push_back("you");
	v3.push_back("can");
	v3.push_back("not");
	CHECK( myMap[v3] == 2);
}
