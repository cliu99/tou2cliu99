/* 
unitTestDriver.cpp

Tingting Ou JHED: tou2

Last modified: Nov. 11, 2016

Description: The Catch framework will provide a driver that calls all the tests.
*/
#define CATCH_CONFIG_MAIN  // This tells Catch to provide a main() - only do this in one cpp file
#include "../include/catch.hpp"
