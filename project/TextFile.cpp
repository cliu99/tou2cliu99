#include "TextFile.h"

TextFile::TextFile(const std::string& filename)
{
    if(filename!="")
        OpenFile(filename);
}

bool TextFile::OpenFile(const std::string& filename)
{
    std::ifstream fin;
    fin.open(filename);
    if(!fin.is_open())
        return false;
    std::string temp;
    while(!fin.eof())
    {
        char c=fin.get();
        if(c>='A' && c<='Z')
            c=c-'A'+'a';

        if(c>='a' && c<='z')
            temp+=c;
        else if(c=='\n')
            temp+='\n';
        else if(temp.size()>0 && temp[temp.size()-1]!=' ')
            temp+=' ';
    }
    text_.str(temp);
    return true;
}

std::vector<std::string> TextFile::getNgram(unsigned n){
    std::vector<std::string> wordArray;//to hold the sentence in the file
    std::string word;
    text_ >> word;//read a word in the file
    while (!text_.eof() && !text_.fail()) {//store words in wordArray; read until we hit EOF
        wordArray.push_back(word);
        text_ >> word;
    }
    std::vector<std::string> NgramCollection;
    for (int i = 0; i<wordArray.size()-n; i++) {
           std::string Ngram;
           for (int j = 0; j<n-1; j++)
                Ngram += wordArray.at(i+j) + " ";
           Ngram += wordArray.at(i+n);
        NgramCollection.push_back(Ngram);
    }
    return NgramCollection;
   
}
    

