/* imageManip.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for image manipulation functions.
 *
 */


//include guard

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

//include necessary C library and header file

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ppmIO.h"
#define PI 3.1415926

/*
*  This is the crop function which takes (x1,y1) as the upper left corner
*  and (x2,y2) as the lower right corner for the crop image.
*  In our program, we decided that x2 has to be bigger than/equal to x1, and 
*  same for y2. We did not like inverted crop and the 
*  assignment did not specify the rules. So we made our rules.
*  Both (x1,y1) and (x2,y2) has to be in the boundary of the original image.
*/
void Crop(Image* myIm, int x1, int y1, int x2, int y2);

/* This is an inversion image function. 
 * Each pixel's r, g, b is equal to the old value substracted from max color value. 
 */
void Inversion(Image* myIm);

/* This is a swap function that swaps the value of green to red, blue to green,
 * and red to blue. This manipulation has to occur after the readImage function.
 * Since we allocate a temporary pixel pointer, we need to free it everything we use it.
 */
void Swap(Image* myIm);

/*This is a function to adjust contrast of an image. 
 *It is done by converting unsigned colors into range [-128, 127] and multiply the value by a contrast factor, then plus 128 back. This manipulation is in another called "Adjustment". After adjustment, we call "Saturation" to make sure that the color value belongs to [0, 255].
 *This manipulation has to occur after the readImage function.
 */
void Contrast(Image* myIm, float cnamt);

/* This is a function to blur the image by a given sigma. 
 * This is done by looking at "a matrix of" values surrounding a pixel's value and set it to a special kind of average (calculated by function "Convolution").
 * This should also occur after the readImage function.
 */
void Blur(Image* myIm, float sigma);

/* This is a function to adjust the brightness of an image.
 * This is done by adding a given brightness amount to each r,g,b value.
 * After addition of value we saturate each value to make sure they belong to range [0, 255].
 * This manipulation has to occur after the readImage function.
 */
void Brighten(Image* myIm, int bramt);

/* This is a function to convert an image into grayscale image. This is done by setting each color value of a pixel tp a given amount (calculated by 0.3*red+0.59*green+0.11*blue)
 * This manipulation has to occur after the readImage function.
 */
void GrayScale(Image* myIm);

/* This is a function to sharpen the image by a given sigma and a given intensity. 
 * This is done by computing the difference between the blurred image and the original image and then subtract the value from the original image. Also, we multiply the difference by intensity to make the sharpening effect stronger/weaker. 
 * This should also occur after the readImage function.
 */
void Sharpen(Image* myIm, float sigma, float intensity);

/* This is a function to rotate the image by 90 degress, clockwise. 
 * This is done by creating a new pixel array, and put corresponding value of the original image into the new pixel array.
 * This should also occur after the readImage function.
 */
void Rotate(Image* myIm);

/* This is a function to apply rainbow stripe effect to an image.
 * This is done by seperating an image into 7 parts, and change the r/g/b values in each part by a given formula to create red, orange, yellow stripes, etc. 
 * This should also occur after the readImage function.
 * */
void Stripe(Image* myIm);

int didRead(Image* myIm);

//Adjustment function used for "Constrast" function to increase/decrease contrast 
//by subtracting 128 from the value, multiply the result by contrast amount and 
//then add 128 back
double Adjustment(int color, float cnamt);

//Saturation function to make sure a value is an integer between 0 and 255
int Saturation(double color);

double Convolution(int color, Image* myIm, int i, int width, float sigma);

//math helper function to get the square of a double
double sq(double x);




#endif 

