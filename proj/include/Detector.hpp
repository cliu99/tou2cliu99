#ifndef DETECTOR_HPP
#define DETECTOR_HPP

#include "NgramCollection.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <list>
#include <algorithm>
#include <locale>  
#define threshold_H 12
#define threshold_M 17
#define threshold_L 22
#define n_h 3
#define n_m 4
#define n_l 5

class Detector{
	public:
	Detector(std::string command);
	std::vector<std::string> readList(std::string filelistname);
	int Compare(std::string file1, std::string file2);
	NgramCollection getColl(std::string file);
	std::string printPair();
		
	private:
	std::string order;
	std::vector<std::pair<std::string, std::string>> simFile;
};


#endif
