/*
NgramCollection.cpp

Author: Tingting Ou JHED: tou2 

Written: Nov. 12, 2016
Last modified: Nov. 30, 2016

Description: Functions needed for class NgramCollection to increment Ngrams, get number N, display details of all Ngrams according to a specific order and pick the next word according to first (n-1) words 
*/

//include header file
#include "../include/NgramCollection.hpp"

	/*Increase count for NgramCollection representing values from begin up to end
	begin is an iterator to the first word in the Ngram,
	end is an iterator to the end of the Ngram (so (end - begin) == N)*/
	void NgramCollection::increment(std::vector<std::string>::const_iterator begin, std::vector<std::string>::const_iterator end) {
			std::vector<std::string> Ngram;// the words to follow in Ngram
			//Store all the n-1 words to our leadingWords array
			for (auto it = begin; it != end; it++) {
				Ngram.push_back(*it);	
			}
			auto find = counts.find(Ngram);//try find the next word in the map
			if (find != counts.end()) {//if we find this next word in map, just add the count
				counts[Ngram]++;
			} else {//if we fail to find this next word in map, create an item in the map
				counts[Ngram]=1;
			}
	}
	
	 
	std::map<std::vector<std::string>, unsigned> NgramCollection::getMap() {
		return counts;
	}
