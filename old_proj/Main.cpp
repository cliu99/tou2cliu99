//Last modified: Dec. 1

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <map>
#include <tuple>
#include <list>
#include <algorithm>
#define threshold_H 12
#define threshold_M 12
#define threshold_L 18
#define n_h 3
#define n_m 5
#define n_l 5
#include "NgramCollection.hpp"

using namespace std;

class Detector{
	public:
	Detector(string command);
	vector<string> readList(string filelistname);
	double Compare(string file1, string file2);
	NgramCollection getColl(string file);
	string printPair();
		
	private:
	string order;
	vector<pair<string, string>> simFile;
};

Detector::Detector(string command) {
	order = command;
}

double Detector::Compare(string file1, string file2){
	NgramCollection c1 = getColl(file1);	
	NgramCollection c2 = getColl(file2);	
	auto map1 = c1.getMap();
	auto map2 = c2.getMap();
	double count = 0.0;
	for (auto it1 = map1.begin(); it1 != map1.end(); it1++) {
	    if (map2.find(it1->first)!=map2.end()) {
			count++;;	
		}
	}
	if (order == "l") {
		if (count >= threshold_L) {
			pair<string, string> simPair (file1, file2);
			simFile.push_back (simPair);
		}
	} else if (order == "m") {
		if (count >= threshold_M) {
				pair<string, string> simPair (file1, file2);
				simFile.push_back (simPair);
		}
	} else {
		if (count >= threshold_H) {
					pair<string, string> simPair (file1, file2);
					simFile.push_back (simPair);
		}
	}
	return count;
}


string Detector::printPair(){
	stringstream ss;
	string level;
	if (order == "m")
		level = "Medium";
	if (order == "l")
		level = "Low";
	if (order == "h")
		level = "High";
	ss << "Suspicious Pairs Detected at " << level << " Level are as followed:\n";
	
	for (auto it = simFile.begin();it != simFile.end(); it++) {
		ss << it->first << " AND " << it->second << '\n';
	}
	return ss.str();
}

vector<string> Detector::readList(string filelistname){
	std::ifstream fin;
	vector<string> myList;
	fin.open(filelistname); //read the filelist which contains names of several files
	if (!fin.is_open()) {//check if we can open the file
		std::cerr << "Cannot open Filelist!\n";
		return myList;
	}
	std::string filename;
	fin >> filename;
	int open = 0;//variable to record number of files we have read
	while (!fin.eof() && !fin.fail()) {//read file names and process each until we hit EOF
		myList.push_back(filename);
		fin >> filename;
		open++;
	}
	if (open == 0){//check if the input filelist is empty
		std::cerr << "Input filelist is empty!\n";
		return myList;
	}
	fin.close();//close the file
	for (auto file1 = myList.begin(); file1!=myList.end(); file1++) {
		for (auto file2 = (file1+1); file2!=myList.end(); file2++) {
				Compare(*file1, *file2);
		}
	}
	return myList;
}

NgramCollection Detector::getColl(string filename) {
	int Nvalue;
	if (order == "m") {
		Nvalue = n_m;
	} else if (order == "l") {
		Nvalue = n_l;
	} else 
		Nvalue = n_h; 
	NgramCollection Coll(Nvalue);
	std::ifstream fin;
	fin.open(filename); //read the file
	if (!fin.is_open()) {//check if we can find and open the file
		std::cerr << "Failed to open this file: " << filename << '\n';
		return Coll;
	}
	std::vector<std::string> wordArray;//to hold the sentence in the file
	std::string word;
	fin >> word;//read a word in the file
	while (!fin.eof() && !fin.fail()) {//store words in wordArray; read until we hit EOF
		wordArray.push_back(word);
		fin >> word;
	}
	if (wordArray.size() == 0){//check if we read nothing from the given file
		return Coll;
	}
		for (std::vector<std::string>::const_iterator it = wordArray.begin(); it != (wordArray.end()-Nvalue+1); it++) {
		Coll.increment(it, it+Nvalue);
	}
	fin.close();//close the file
	return Coll;
}

int main(int argc, char *argv[]) {
	string filelist;
	string order;
	if (argc == 2) {
			filelist = argv[1];
			Detector dec("m");
			vector<string> vec = dec.readList(filelist);
			if (vec.empty()) 
				return 0;
			cout << dec.printPair();
	}
	else if (argc == 3) {
			filelist = argv[1];
			order = argv[2];
			if (order != "h" && order != "m" && order != "l") {
				cout << "Error: Wrong sensitivity command!\n";
				return 0;
			}
			Detector dec(order);
			vector<string> vec = dec.readList(filelist);
			if (vec.empty()) 
				return 0;
			cout << dec.printPair();
	} else {
			cout << "Error: Invalid command line arguments!\n";
	}
	return 0;
}

