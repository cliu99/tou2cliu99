/* ppmIO.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Functions used for reading input image and writing output image.
 */


#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"


//Function used to read an input image
int readimage(Image* myIm, char *filename) {
	

	FILE* ipf = fopen(filename, "rb");//file handle for input image
	char cha;//cha is the character we are currently reading 
	char ppmtag[3] = {'P','6','\0'};//ppmtag is P6, a necessary component of a PPM image
	
	//Check if we can open the file
	if (!ipf) {
	  fprintf(stderr, "Cannot find input image %s!\n", filename);
	  return 0;
	}
	
	//Check if there is a comment in the first lines of the file. 
	while((cha=fgetc(ipf)) != EOF){
	  if (cha == '#') {
	    while ((cha=fgetc(ipf))!='\n');//read the whole line of comment
	  } else {
	      ungetc(cha, ipf);//unget the character for future convenience if we got a character which does not belong to a comment line
	      break;
	  }
	}
	
	//Check twice if the first two characters are exactly "P6"
	for (int i=0; i<2; i++) {
	  if ((cha = fgetc(ipf))== EOF || cha != ppmtag[i] ) {
	    fprintf(stderr, "The given image is NOT a PPM file!\n");
	    return 0;
	  }
	}
	
	//If we have a valid PPM image, print the message saying that we are reading the image
	printf("Reading from %s...\n", filename);
	
	//data field used to store parameters of input PPM image
	int cols = 0;
	int rows = 0;
	int colors = 0 ;

	char ch;//The character we are reading now
	
	for (int i = 0; i < 3; i++) { // Read three parameters into our data field
	  while((ch=fgetc(ipf)) != EOF ){
	    if (isspace(ch)){//if we hit a space, read next character
	      continue;
	    } else if(ch =='#'){//if we hit a comment, read the whole line and continue reading the next character
	        while((ch=fgetc(ipf))!='\n');
	        continue;
            } else {//if we find a parameter, go backwards and scan it
	        ungetc(ch, ipf);
		if (i == 0) {//the first time we read cols
		fscanf(ipf,"%d",&cols);
		} else if (i == 1) {//the second time we read rows
		fscanf(ipf,"%d",&rows);
		} else {//the last time we read colors
		fscanf(ipf,"%d",&colors);
		}
		break;//after we read a parameter, break and go find the next one 
	    }
	  }
	}
	
	//Create a new size Pixel array and copy the parameters from input image)
	myIm->data = realloc(myIm->data, sizeof(Pixel)*cols*rows);
	myIm->rows = rows;
	myIm->cols = cols;
	myIm->colors = colors;
	//read the data into myIm according to the new parameters
	fread(myIm->data, sizeof(Pixel), rows*cols, ipf);
	fclose(ipf);//close file, end processing
		
	return 0;
									
}

//Function used to write a new PPM image
int writeimage(Image* myIm, char* filename) {
  	
	//Check if we have already read an image
	if (myIm->data == NULL) {
	  fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
	  return 0;
	}

	FILE *opf = fopen(filename, "wb");//file handle for opening (or creating) output image
	
	//Check if we can open the file 
	if (!opf) {
	  fprintf(stderr, "Unable to open file %s for writing!\n", filename);
	  return 0;
	}
	
	//If we have an image then we start writing
	printf("Writing to %s...\n", filename);
	
	/* write tags for PPM*/
	fprintf(opf, "P6\n");
	/* write dimensions */
	fprintf(opf, "%d %d %d\n \n", myIm->cols, myIm->rows, myIm->colors);
	/* write pixels */
	int written = fwrite(myIm->data, sizeof(Pixel), (myIm->rows) * (myIm->cols), opf);
	
	//Check if we have successfully written pixel data into the file
        if (written != (myIm->rows * myIm->cols)) {
	  fprintf(stderr, "Failed to write data to file!\n");
	  fclose(opf);
	  return 0;
	}

	fclose(opf);//close file and end processing

	return 1;
}


