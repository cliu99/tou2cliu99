#include<iostream>
#include<string>
#include<list>
#include<fstream>
#include<map>
#include<vector>
#include<iomanip>
using namespace std;
class Compare
{
public:ifstream infile1,infile2;
	   list<string> arc,arc1,ard,ard1;
	   map<string,int> data1,data2;
	   map<string,vector<int> > pos1,pos2;
	   list<string> samewords,same;
	   map<int,int> naborsamewords;
	   map<int,int> fangcha;
	   void create()
	   {
		   string file,words;
		   //cout<<"请输入第一个文件名："<<endl;getline(cin,file);
		   infile1.open("control.txt",ios::in);
		   //infile1.open(file,ios::in);
		   while(!infile1.eof())
		   {
			   infile1>>words;
			   size_t p1;
			   p1=test(words);
			   if(p1!=string::npos)
			   {
				   string words1,words2;
				   words1.assign(words,0,int(p1));arc.push_back(words1);
				   words2.assign(words.begin()+int(p1)+1,words.end());arc.push_back(words2);
			   }
			   else
			   {
				   arc.push_back(words);
			   }  
		   }
		   arc1.assign(arc.begin(),arc.end());
		   //cout<<"请输入第二个文件名："<<endl;getline(cin,file);
		   infile2.open("control1.txt",ios::in);
		   //infile2.open(file,ios::in);
		   while(!infile2.eof())
		   {
			   infile2>>words;
			   size_t p1;
			   p1=test(words);
			   if(p1!=string::npos)
			   {
				   string words1,words2;
				   words1.assign(words,0,int(p1));ard.push_back(words1);
				   words2.assign(words.begin()+int(p1)+1,words.end());ard.push_back(words2);
			   }
			   else
			   {
				   ard.push_back(words);
			   }
		   }
		   ard1.assign(ard.begin(),ard.end());
	   }
	   size_t test(string s)
	   {
		   size_t t;
		   t=s.find(',');if(t!=string::npos){return t;}
		   t=s.find('.');if(t!=string::npos){return t;}
		   t=s.find('?');if(t!=string::npos){return t;}
		   t=s.find('!');if(t!=string::npos){return t;}
		   t=s.find(':');if(t!=string::npos){return t;}
		   t=s.find('\'');if(t!=string::npos){return t;}
		   t=s.find('\"');if(t!=string::npos){return t;}
		   t=s.find("……");if(t!=string::npos){return t;}
		   t=s.find("......");if(t!=string::npos){return t;}
		   t=s.find("。");if(t!=string::npos){return t;}
		   t=s.find("<");if(t!=string::npos){return t;}
		   t=s.find(">");if(t!=string::npos){return t;}
		   t=s.find("“");if(t!=string::npos){return t;}
		   t=s.find("-");if(t!=string::npos){return t;}
		   t=s.find("”");if(t!=string::npos){return t;}
		  // t=s.find("，");if(t!=string::npos){return t;}
		   return string::npos;
	   }
	   void getdata()
	   {
		   int a=0;string s;
		   while(!arc1.empty())
		   {
			   s=arc1.front();a=countwords1(s);
			   data1[s]=a;
		   }
		   while(!ard1.empty())
		   {
			   s=ard1.front();a=countwords2(s);
			   data2[s]=a;
		   }
	   }
	   int countwords1(string s)
	   {
		   int a=0; 
		   list<string>::iterator it;
		   for(it=arc1.begin();it!=arc1.end();it++)
		   {
			   if(s.compare(*it)==0)
			   {
				   a++;
			   }
		   }
		   arc1.remove(s);
		   return a;
	   }
	   int countwords2(string s)
	   {
		   int a=0; 
		   list<string>::iterator it;
		   for(it=ard1.begin();it!=ard1.end();it++)
		   {
			   if(s.compare(*it)==0)
			   {
				   a++;
			   }
		   }
		   ard1.remove(s);
		   return a;
	   }
	   void getpos()
	   {
		   map<string,int>::iterator it1;list<string>::iterator it2;
		   string s1;
		   for(it1=data1.begin();it1!=data1.end();it1++)
		   {
			   int a=0;vector<int> pos11;s1=(*it1).first;
			   for(it2=arc.begin();it2!=arc.end();it2++,a++)
			   {
				   if(s1.compare(*it2)==0)
				   {
					   pos11.push_back(a);
				   }
			   }
			   this->pos1[s1]=pos11;
		   }
		    for(it1=data2.begin();it1!=data2.end();it1++)
		   {
			   int a=0;vector<int> pos22;s1=(*it1).first;
			   for(it2=ard.begin();it2!=ard.end();it2++,a++)
			   {
				   if(s1.compare(*it2)==0)
				   {
					   pos22.push_back(a);
				   }
			   }
			   this->pos2[s1]=pos22;
			}
	   }
	   void findsame()
	   {
		   string s1;
		   map<string,int>::iterator it1,it2;
		   for(it1=data1.begin();it1!=data1.end();it1++)
		   {
			   s1=(*it1).first;it2=data2.find(s1);
			   if(it2!=data2.end())
			   {
				   samewords.push_back(s1);
			   }
		   }
	   }
	   int comparevector(vector<int> a,vector<int> b)//方差
	  {
		  int de,l,i,sum=0,aver,c,d;vector<int>de2;
		  l=a.size()<b.size()?a.size():b.size();
		  for(i=0;i<l;i++)
		  {
			  d=(a.at(i)-b.at(i));
			  de=d>0?d:(-d);de2.push_back(de);sum+=de;
		  }aver=sum/l;sum=0;
		  for(i=0;i<l;i++)
		  {
			  c=(de2.at(i)-aver)*(de2.at(i)-aver);sum+=c;
		  }
		  return sum/l;
	  }
	   list<string>::iterator tranverse1(int i)
	   {
		   if(i>arc.size()){return arc.end();}
		   list<string>::iterator it;
		   it=arc.begin();int s=0;
		   while(s<i)
		   {
			   it++;s++;
		   }
		   return it;
	   }
	   list<string>::iterator tranverse2(int i)
	   {
		   if(i>ard.size()){return ard.end();}
		   list<string>::iterator it;
		   it=ard.begin();int s=0;
		   while(s<i)
		   {
			   it++;s++;
		   }
		   return it;
	   }
	   int countnabor(int a,int b)//统计相同单词前后各2个单词的相同单词数
	   {
		   int s=0;
		   list<string>::iterator it1,it2;
		   for(it1=tranverse1(a),it2=tranverse2(b);it1!=tranverse1(a+5)&&it2!=tranverse2(b+5);it1++,it2++)
		   {
			   if(it1!=arc.end()&&it2!=ard.end()&&(*it1).compare((*it2))==0){s++;}
		   }
		   return --s;
	   }
	   void vectorsort(vector<int> v)
	   {
		   int i,temp,p;
		   for(p=0;p<v.size();p++)
		   {
			   temp=v[p];i=p-1;
			   while(i>=0&&v[i]>temp)
			   {
				   v[i+1]=v[i];i--;
			   }v[i+1]=temp;
		   }
	   }
	   int countnaborsum(vector<int>a,vector<int>b)
	   {
		   vectorsort(a);vectorsort(b);int sum=0;
		   vector<int>::iterator it1,it2;int i,j=a.size()<b.size()?a.size():b.size();
		   for(i=0;i<j;i++)
		   {
			   sum+=countnabor(a[i],b[i]);
		   }
		   return sum;
	   }
	   void database()
	   {
		   int len,sum=0;int a,b,c;string s;vector<int> v1,v2;same.assign(samewords.begin(),samewords.end());
		   while(!samewords.empty())
		   {
			   s=samewords.front();
			   a=data1[s];b=data2[s];
			   len=a<b?a:b;
			   v1=pos1[s];
			   v2=pos2[s];
			   sum=countnaborsum(v1,v2);c=comparevector(v1,v2);fangcha[len]=c;
			   naborsamewords[len]=sum;
			   samewords.pop_front();
		   }
	   }
	   void show()
	   {
		   this->create();getdata();getpos(); findsame();database();double s1,s2;int value=0,value1=0;
		   list<string>::iterator it1;string s;int l;
		   cout<<"文章1："<<endl;
		   for(it1=arc.begin();it1!=arc.end();it1++){cout<<*it1<<' ';}cout<<endl;
		   cout<<"文章2："<<endl;
		   for(it1=ard.begin();it1!=ard.end();it1++){cout<<*it1<<' ';}cout<<endl;
		   cout<<"两篇文章中相同的单词+文章1中出现的次数+文章2中出现的次数+单词后4个单词中的相同单词个数和+位置方差："<<endl;
		   for(it1=same.begin();it1!=same.end();it1++)
		   {s=*it1;l=data1[s]<data2[s]?data1[s]:data2[s];value1+=naborsamewords[l]/l;
			   cout<<setw(10)<<*it1<<' '<<setw(4)<<data1[s]<<' '<<setw(4)<<data2[s]<<' '<<setw(4)<<naborsamewords[l]<<' '<<setw(4)<<fangcha[l]<<endl;
			   if(fangcha[l]<=1){value++;}
		   }s1=value/same.size()*0.3*100;
		   s2=(value1*100)/(same.size()*4)*0.7;
		   cout<<"相似度："<<s1+s2<<'%'<<endl;
       }
};
int main()
{
	Compare C;
	C.show();
	return 1;
}