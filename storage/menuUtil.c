/* menuUtil.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Functions used to print the menu and read user input
 *
 */


#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"


//Print menu function for user.
int printmenu(){
	printf("Main menu:\n\tr <filename> - read image from <filename>\n\t");
	printf("w <filename> - write image to <filename>\n\t");
	printf("c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n\t");
	printf("i - invert intensities\n\t");
	printf("s - swap color chanels\n\t");
	printf("g - convert to grayscale\n\t");
	printf("bl <sigma> - Gaussian blur with the given radius (sigma)\n\t");
	printf("sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n\t");
	printf("br <amt> - change brightness (up or down) by the given amount\n\t");
	printf("cn <amt> - change contrast (up or down) by the given amount\n\t");
	printf("o - rotate the picture by 90 degrees, clockwise\n\t");
	printf("t - apply a rainbow stripe effect to the picture\n\t");
	printf("q - quit\n");
	printf("Enter choice: ");	
	return 0;
}


//The main menu funtion to read input and call corresponding functions
int mainmenu() {
	printmenu();
	int length = 50;//MAX buffer size in the first place
	char* buffer = malloc(sizeof(char) * length);//buffer word to store file name string
	Image myIm;//My image
	myIm.data = NULL;//initialize the pixel field pointer to NULL
	char ch; //user input.
	//Read the user input until we hit EOF or 'q'
	while((ch = getchar())!=EOF && ch != 'q'){
		 if (ch == 'r' || ch == 'w') { //if we got r or w, call function to read the input and call functions
		 	int check = readOrWrite(ch, &myIm, buffer, &length);
		 	if (check == -1){ //we don't need to free buffer and create a new one if check is -1
		 		continue;
		 	} 
		 	char *empbuffer = malloc(sizeof(char)*length);//after using the buffer we free it and create a new one
			if (buffer!=NULL){
				free(buffer); 
			} 
			buffer = empbuffer;
		 } else if (ch == 'c') { //now if the user enters 'c' prepare to do crop/contrast according to the next character
			cropOrContrast(ch, &myIm, buffer);
		 } else if (ch == 'g' || ch == 'i'|| ch =='o' || ch == 't'){//if the user enters 'g'/'i'/'o'/'t' prepare to call grayscal/invert/rotate/stripe
			grayOrInvertOrRotateOrStripe(ch, &myIm, buffer);
		 } else if ( ch == 's'){//if the user enters 's' then prepare to swap
			sharpenOrSwap(ch, &myIm, buffer);
		 } else if (ch == 'b'){ //if the user enters 'b' then prepare to do blur or brighten according to the next character
			blurOrBrighten(ch, &myIm, buffer);
		 } else { //if the user has not entered anything like those on the menu.
			  	if (!isspace(ch)) {//if we got a character that's not on the menu, print error message
			    	fprintf(stderr, "No such Command! Enter again!\n");
			    	while((ch=getchar())!='\n');//read the whole line then
					printmenu();
			  	} else{//if we got some spaces, continue to get following characters
			  	 	continue;
			  	}
		 }
    }    
    //if the user has entered q or reaches EOF.
	printf("Goodbye!\n");
	if (buffer!= NULL) 
		free(buffer);//free the memory we have used
	if (myIm.data != NULL)
		free(myIm.data);//free the image
	return 0;
}



//This function will print Goodbye and free memory when the user accidentally hit EOF in stdin.
void printGoodbye(char* buffer, Image *myIm){
	printf("Goodbye!\n");
	if (buffer!= NULL) {//free the buffer
		free(buffer);
	}i
	if ((myIm->data)!=NULL) {//free the image
		free(myIm->data);
	}
}

/*Functions to read user input and to call correspondent functions according to the characters we have read*/
/*Classied by the first character we get in a line*/


/* If we read a "r" or "w", then call this function to first check
 * if the input is valid. If it is valid, then this function will
 * call the corresponding functions for read/write an image in imageManip.c
 */
int readOrWrite(char ch, Image *myIm, char* buffer, int* length){
	int rw = 0; //this is a flag to tell if the input is readImage or writeImage.
	int invalid = 0;//used for checking invalid input
	int i = 0;//index used to store buffer characters

	if (ch == 'r'){//rw is 1 when we need to read
		rw = 1;
	}
	if (ch == 'w'){//rw is -1 when we need to write
		rw = -1;
	}

	ch = getchar();//get the next character to start checking

	//if a user types r/w and adds characters following r/w, it is invalid.
	if (!(isspace(ch))) {
		invalid = 1;
		while((ch=getchar())!='\n');
	}
		
	//a user may type multiple spaces before the next char.
	while(isspace(ch)){
	//still invalid if the user type r/w and a bunch of spaces and hits enter.
		if (ch == '\n'){
			invalid = 1;
			break;
		}
		ch = getchar();
	}
	//If invalid input, then return -1 to show that we have not used the buffer
	if (invalid == 1){
		fprintf(stderr, "Invalid input!\n"); 
		printmenu();
		return -1;
	}
	//move the cursor back a char to get the first character of the filename.    
	ungetc(ch, stdin);
 	while (!isspace(ch=getchar())) {
		//if the input reaches EOF. Exit.
        if (ch == EOF){
			printGoodbye(buffer, myIm);
			return 0;
		}
		//if the input is too large. Reallocate the memory for the buffer string.	
		if (i == (*length)-2) {
			*length = (*length) * 2;
			buffer = realloc(buffer, sizeof(char) * (*length));
			buffer[i++] = ch;
		} else {
			buffer[i++] = ch;
		  }	
	}
	buffer[i]='\0';//Terminate our buffer word

	//the user should not enter any chars afterwards (aka only spaces and \n follow.)
	int invalidcheck = 0;
	if (ch != '\n') {//if there are inputs after the filename before a newline
		while ((ch = getchar())!='\n') {//read the whole line
			if (!isspace(ch)) {
	  		invalidcheck = 1;
			}
		}
	//if the user did input something after the filename, it is invalid.
	//discard the content of the buffer.
		if (invalidcheck == 1) {
			fprintf(stderr, "Invalid input!\n"); 
			printmenu();
			return 0;
		}
	}

	if (rw == 1){//if we have a read flag and valid input filename, read the image
		readimage(myIm, buffer);
	}
	if (rw == -1){//if we have a write flag and valid output filename, write the image
	  	writeimage(myIm, buffer); 
	}
	printmenu();
	return 0;
}

/* If the user enters 'c', this function first checks if the 
 * user has entered a valid input for either crop or contrast.
 * If the input is invalid, this function print out an error 
 * message and prompt the menu list again for the user. If the 
 * input is valid, this function then calls the crop/contrast function
 * in imageManip.c to execute.
 */
int cropOrContrast(char ch, Image *myIm, char* buffer){
		//Get the next character. Check if we hit EOF
		if((ch = getchar())==EOF) {
		    printGoodbye(buffer, myIm);
			return 0;
		}
		//If we got 'n' we do operations to prepare for constrast function
		if (ch == 'n') {
		  ch = getchar();//check the character after "cn"
		  if (ch =='\n' || !isspace(ch)) {//Check if there is no input after "cn" 
			if (ch!='\n') {//if there is an invalid letter or number after "cn", read the whole line
				while((ch=getchar())!='\n');
			}
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return -1;
		  }   
		  ungetc(ch, stdin);//if the input is valid, unget to scanf numbers

		  float cnamt;
		  int scan = scanf("%f",&cnamt);
		  //if scanf gets EOF.
		  if (scan == -1) {
				printGoodbye(buffer, myIm);
				return 0;
		  } else if (scan == 0) { 
		    //if scanf did not get a float for amount.
		        while((ch=getchar())!='\n');//read the whole line first
				fprintf(stderr, "Invalid input!\n");
				printmenu();
				return -1; 
		  } else {//if scanf gets a number successfully
		        ch = getchar();
		        int invalid = 0;//new input check variable defined for following checks
			
			//check if the inputs after cn has chars or EOF.
			while(ch!= '\n') {//read until the newline character
				if (ch == EOF) {
					printGoodbye(buffer, myIm);
					return 0;
				}
				if (!isspace(ch)) {
					invalid = 1;
				}
				ch = getchar();
			}
			if (!invalid) {//If we pass each test, then we call Contrast functions
				Contrast(myIm, cnamt);
			}
			else { 
			    fprintf(stderr, "Invalid input!\n");
			}
		  printmenu();
		  return 0;
		  }
		} else if (ch == '\n') {//if we only get a c, then the input is invalid 
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return -1;
		} else if (!isspace(ch)) { //anything other than 'c '(e.g cb, ca) should be invalid.
		  	fprintf(stderr, "Invalid input!\n");
		        while(((ch=getchar())!= '\n')) {//then we read the whole line
		    	  if(ch==EOF) {
			      printGoodbye(buffer, myIm);
			      return 0;
		   	    }
			}
		  	printmenu();
		  	return -1;
		} else {//if have have a space after 'c' we prepare to do operations for cropping
		  int array[4];//used to store numbers scanned
	          int scans = scanf("%d %d %d %d", &array[0], &array[1],&array[2], &array[3]);
		  if (scans != 4) {//if our scans fail
		  	if (scans == -1){//if we hit EOF
			  	printGoodbye(buffer, myIm);
				return 0;
		  	}
			//if the input is invalid but not a EOF, we read the whole line
		  	fprintf(stderr, "Invalid input!\n");
		    
		  	while(((ch=getchar())!= '\n')) {
		       		if (ch == EOF) {
					printGoodbye(buffer, myIm);
					return 0;
		      		}
		      	}
			printmenu();
			return -1;
		  }
		  //if scanf successfully get 4 integers. 
		  //Read till \n to check if the user has entered additional invalid inputs.
		  ch = getchar();
		  int invalid = 0;//new variable for checking invalid input after 4 integers
		  while(ch!= '\n') {
			    if (ch == EOF) {
				printGoodbye(buffer, myIm);
				return 0;
			    }
			    if (!isspace(ch)) {
			    	invalid = 1;
			    }
			ch = getchar();
		  }

		  if (!invalid) {//if the input is okay, crop the image
            		Crop(myIm,array[0], array[1], array[2], array[3]);
		  } else { //if input is invalid, print an error message
		    	fprintf(stderr, "Invalid input!\n");
		  }
		printmenu();
		return 0;
		}
}
/* This function is called when the user enters 'g', or 'i', or 'o'. or 't'.
 * It first check if the input is valid. If the input is invalid, it prints out
 * an error message and prompt the user again. If the input is valid, it calls 
 * the corresponding function in imageManip.c (grayscale, inversion, rotation, or stripe function).
 */
int grayOrInvertOrRotateOrStripe(char ch, Image *myIm, char* buffer){
	//again gi is another flag for g(grayscale)/i(invert)/o(rotate 90 degrees clockwise)/t(stripe effect).
	int igot = 0;//0 means invert, 1 means grayscale, 2 means rotate, 3 means stripe
	int invalid = 0;//invalid is for checking additional input after g/i
	if (ch == 'g'){
		igot = 1;
	} else if (ch == 'o') {
		igot = 2;
	} else if (ch == 't') {
		igot = 3;
	}
	//no extra chars should appear after g/i till \n.
	while((ch=getchar())!='\n'){
		if (ch == EOF) {
		    printGoodbye(buffer, myIm);
		    return 0;
		}
		if(!isspace(ch)){
			invalid = 1;
		}
	}

	if(!invalid){//if the input is okay
		if (igot == 1){//if gi is 1, then it means we do grayscale
			GrayScale(myIm);
		} else if (igot == 2) {//if gi is 2, then it means we do rotation
			Rotate(myIm);
		} else if (igot == 3) {//if gi is 3, then it means we do rainbow stripe effect
			Stripe(myIm);
		} else  {//if gi is 0, then it means we do inversion
			Inversion(myIm);
		}
	} else {//if invalid, print error message and menu, go back to get the next character
		fprintf(stderr, "Invalid input!\n");
	}
	printmenu();
	return 0;
}

/* This function is called when the user enters 's'. 
 * It first checks if the input is valid and if the user meant
 * sharpen or swap channels. If the input is invalid, it prints
 * out an error message and prompt the user again. If the input is valid,
 * it calls the corresponding functions in imageManip.c to execute.
 */
int sharpenOrSwap(char ch, Image *myIm, char* buffer){
	ch = getchar();//the character we are reading now
	int invalid = 0;//flag to check if the input is valid or not
		if (ch == 'h' || isspace(ch)){//if we have an 'h' or space after 's' then the input may be valid
			if(ch == 'h'){ //if the user wants sh
				ch = getchar();//get the character after "sh"
				if (isspace(ch) && ch != '\n'){//if there is a non-newline space after sh, prepare to do sharpening
					float shsigma;//sigma for sharpening
					float shamt;  //amount for sharpening
					int scan = scanf("%f %f", &shsigma, &shamt);
					
					if (scan == -1){ //if scanf hits EOF, exit the program
					printGoodbye(buffer, myIm);
					return 0;
					}
					while((ch=getchar())!='\n'){ //check the input after the scanned number
							if (ch == EOF){//if hit EOF, exit the program
								printGoodbye(buffer, myIm);
								return 0;
							}
							if (!isspace(ch)) {//if there is input after the scanned number, the input is invalid
								invalid = 1;
							}
						}
					if (invalid == 1 || scan != 2) {//if the input is invalid or scan fails
						fprintf(stderr, "Invalid input!\n");
					} else {//if the input is valid and scan succeeds
						Sharpen(myIm, shsigma, shamt);
					}
					printmenu();
					return 0;
				} else {//if there is an invalid character after h or there is a newline after h
					if (ch != '\n') {
							while((ch=getchar())!='\n');
					}
					printf("Invalid input!\n");
					printmenu();
					return 0;
				}
			} else {// if the user wants 's' we may need the swap function, prepare to check and call it
				while(ch!='\n'){//read the whole line to check if there is invalid input
					if(!isspace(ch)){//if we detect invalid input after s
						invalid = 1;
			        }
					ch = getchar();
				}
				if(invalid == 0){//if the input is valid, we swap the image
					Swap(myIm);
				} else {//if the input is invalid, we print an error message
					fprintf(stderr, "Invalid input!\n");
				}
				printmenu();//print menu and go back to get the next character
				return 0;
			} 
		} else { //if the user types an invalid input starting with 's'.
		    if (ch != '\n') {
			while ((ch=getchar())!='\n');//read the whole line
			}
			fprintf(stderr, "Invalid input! Please check the input format.\n");
			printmenu();
			return 0;
		}
}

/* This fucntion is called when the user enters 'b'.
 * It will first check if the input is valid or not.
 * If invalid, it will print an error message and prompt again.
 * If valid, it will call the corresponding functions in imageManip.c,
 */
int blurOrBrighten(char ch, Image *myIm, char* buffer) {
    ch = getchar();
    int flag = 0;
    if (ch == 'r' || ch == 'l'){
		if (ch == 'r')
		 flag = 1;  
		if (ch == 'l')
		 flag = -1; 

		ch = getchar();//get the third character in the line -- if input is valid it should be a non-newine space

		if (isspace(ch) && (ch != '\n')){//if the 3nd character is fine
		    int bramt;
		    float blamt;  
		    int scan;
		    if (flag == 1)
				scan = scanf("%d", &bramt);
	        else
				scan = scanf("%f", &blamt);

		    if (scan == -1) {
			printGoodbye(buffer, myIm);		
			return 0;
		    }
		
		    int invalid = 0;
		
		    while((ch=getchar())!='\n'){// read the whole line to check if there is input after the parameter
				if (ch == EOF) {
					printGoodbye(buffer, myIm);		
					return 0;
				}
				if (!(isspace(ch))){
		    		invalid = 1;
		    	}
		    }
		
		    if (scan == 1 && invalid == 0){
		        if (flag == 1) {
					Brighten(myIm, bramt);
				}
				if (flag == -1) {
					Blur(myIm, blamt);
				}
				printmenu();
				return 0;
		    } else { //if our scan fails or there is input after the scanned number
			fprintf(stderr, "Invalid input! Please check the format of parameters.\n");
			printmenu();
			return 0;
		    }
		} else {//if the 3rd character is a newline (void of parameter) or if it is a non-space invalid character
			if (!isspace(ch))
				while((ch=getchar())!='\n');
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return 0;
		}
    } else {//if the second character is neither r nor l, read the whole line and print error message
	if (ch != '\n')	{
		while((ch=getchar())!='\n');
	}
	fprintf(stderr, "Invalid input! Please specify function you need and add parameters..\n");
	printmenu();
	return 0;
    }
}


