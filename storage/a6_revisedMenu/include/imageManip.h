/* imageManip.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for image manipulation functions.
 *
 */


//include guard

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

//include necessary C library and header file

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"

//Function Prototypes go here

//Crop images according to two corners given
int Crop(Image* myIm, int x1, int y1, int x2, int y2);
//Invert the image
int Inversion(Image* myIm);
//Swap the color chanel of the image
int Swap(Image* myIm);

//Functions to be written in phase 2
void Contrast();
void Blur();
void Brighten();
void GrayScale();
void Sharpen();

#endif 

