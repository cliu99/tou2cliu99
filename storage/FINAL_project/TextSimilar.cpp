#include "TextSimilar.h"
#include <cmath>
#include<iostream>
TextSimilar::TextSimilar()
{

}

//初始化数据
void TextSimilar::InitData(const std::vector<std::string>& filename)
{
    length_.clear();
    dict_.clear();

    length_.resize(filename.size());


    for(std::size_t i=0;i<filename.size();++i)
    {
        length_[i]=0;
        TextFile textFile;
        if(!textFile.OpenFile(filename[i]))
            continue;

        while(textFile.HasNext())
        {
            HashString str(textFile.GetNext());
            if(dict_.count(str))
            {
                ++dict_[str][i];
            }
            else
            {
                std::vector<unsigned>temp(filename.size(),0);
                temp[i]=1;
                dict_[str]=std::move(temp);
            }
        }
        for(const std::pair<HashString,std::vector<unsigned> >& temp:dict_)
            length_[i]+=temp.second[i]*temp.second[i];
        length_[i]=sqrt(length_[i]);
    }

}

//计算文档相似度
double TextSimilar::ComputeSimilar(std::size_t x,std::size_t y)
{
    double ret=0;
    for(const std::pair<HashString,std::vector<unsigned> >& temp:dict_)
        ret+=temp.second[x]*temp.second[y];
    ret/=length_[x]*length_[y];
    return ret;
}


//获取认为抄袭的文档对(坐标表示)
std::vector<std::pair<std::size_t,std::size_t>> TextSimilar::GetSimilar(const std::vector<std::string>& filename,char mode)
{
    std::vector<std::pair<std::size_t,std::size_t>> ret;
    HashString::SetMode(mode);
    InitData(filename);
    for(std::size_t i=0;i<filename.size();++i)
        for(std::size_t j=i+1;j<filename.size();++j)
        {
            double val=ComputeSimilar(i,j);
//            std::cout<< filename[i]<<' '<<filename[j]<<' '<<val<<std::endl;
            bool isSimilar=true;
            switch(mode)
            {
            case 'l':
                isSimilar=val>0.9;
                break;
            case 'm':
                isSimilar=val>0.85;
                break;
            case 'h':
                isSimilar=val>0.8;
                break;
            }
            if(isSimilar)
            {
                ret.push_back(std::make_pair(i,j));
            }
        }
    return ret;
}
