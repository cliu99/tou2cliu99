#include "HashString.h"

char HashString::mode_='m';

HashString::HashString(const std::string& str)
    :str_(str)
{
    hash_ = CountHash();
}

bool HashString::operator == (const HashString& temp)const
{
    if(HashString::mode_!='h')
        return hash_ == temp.hash_;
    else
        return str_==temp.str_;
}

bool HashString::operator < (const HashString& temp)const
{
    if(HashString::mode_!='h')
        return hash_ < temp.hash_;
    else
        return str_ < temp.str_;
}

void HashString::SetMode(char mode)
{
    HashString::mode_=mode;
}

//计算字符串hash值
unsigned HashString::CountHash()const
{
    if(HashString::mode_=='h')return 0;
    //113025431一个大素数 * 26不会溢出
    unsigned mod=113025431;
    //不敏感的话取一个小素数
    if(HashString::mode_=='l')
        mod=65537;
    unsigned ret=0;
    for(char c:str_)
    {
        ret = (ret+c-'a')%mod;
        ret = (ret*26)%mod;
    }
    return ret;
}

std::string HashString::ToString()const
{
    return str_;
}

unsigned HashString::GetHash()const
{
    return hash_;
}
