#ifndef TEXTSIMILAR_H
#define TEXTSIMILAR_H
#include <vector>
#include <map>
#include <tuple>
#include "TextFile.h"
#include "HashString.h"




class TextSimilar
{
public:
    TextSimilar();

    //获取认为抄袭的文档对(坐标表示)
    std::vector<std::pair<std::size_t,std::size_t>> GetSimilar(const std::vector<std::string>& filename,char mode='m');

private:
    void InitData(const std::vector<std::string>& filename);
    double ComputeSimilar(std::size_t x,std::size_t y);
    std::map<HashString,std::vector<unsigned> >dict_;
    std::vector<double> length_;
};

#endif // TEXTSIMILAR_H
