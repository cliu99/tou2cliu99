#ifndef HASHSTRING_H
#define HASHSTRING_H

#include <string>

class HashString
{
public:
    HashString(const std::string& str="");

    void static SetMode(char mode);

    bool operator == (const HashString& temp)const;

    bool operator < (const HashString& temp)const;

    unsigned GetHash()const;

    std::string ToString()const;

    static char mode_;

private:
    unsigned CountHash() const;
    std::string str_;
    unsigned hash_;

};


#endif // HASHSTRING_H
