/* imageManip.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for image manipulation functions.
 *
 */


//include guard

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

//include necessary C library and header file

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "ppmIO.h"
#define PI 3.1415926

//Function Prototypes go here

//Crop images according to two corners given
int Crop(Image* myIm, int x1, int y1, int x2, int y2);
//Invert the image
int Inversion(Image* myIm);
//Swap the color chanel of the image
int Swap(Image* myIm);

//Functions to be written in phase 2
void Contrast(Image* myIm, float cnamt);
void Blur(Image* myIm, float sigma);
void Brighten(Image* myIm, int bramt);
void GrayScale(Image* myIm);
void Sharpen(Image* myIm, float sigma, float intensity);
void SaltAndPepperNoise(Image* myIm);

int didRead(Image* myIm);
double Adjustment(int color, float cnamt);
int Saturation(double color);
double Convolution(int color, Image* myIm, int i, int width, float sigma);

double sq(double x);




#endif 

