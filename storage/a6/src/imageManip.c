/* imageManip.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Functions used for manipulating and processing image.
 */


#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"

int didRead(Image *myIm){
  if (myIm->data == NULL) {//Check if we have read an image
    fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
    return 0;
  }
  return 1;
}
/*
*  This is the crop function which takes (x1,y1) as the upper left corner
*  and (x2,y2) as the lower right corner for the crop image.
*  In our program, we decided that x2 has to be bigger than/equal to x1, and 
*  same for y2. We did not like inverted crop and the 
*  assignment did not specify the rules. So we made our rules.
*  Both (x1,y1) and (x2,y2) has to be in the boundary of the original image.
*/
int Crop(Image* myIm, int x1, int y1, int x2, int y2) {
  if (x2<=x1 || y2<=y1) {//check is we have invalid input
	fprintf(stderr, "Invalid dimension input!\n");
	return 0;
	}
  int read = didRead(myIm);
  if (read){
  //rows and cols for the cropped image.	
  int r = x2 - x1;
  int c = y2 - y1;

  int newsize = r * c;
  int rows = myIm->rows;
  int cols = myIm->cols;

  int index = 0;
  //checks if the two corners are inside the image boundary.
  if ((x2>(rows-1)) || (y2>(cols-1))) {
  fprintf(stderr, "Dimensions indicated out of bound!\n");
  return 0;
  }

  printf("Cropping region from (%d, %d) to (%d, %d)...\n",x1,y1,x2,y2);

  Pixel* newdata = malloc( sizeof(Pixel) * newsize );//new data field to store pixels

  for (int i = x1; i<x2; i++) { //copy pixels to new pixel data field
    for (int j = y1; j<y2;j++) {
      newdata[index++] = (myIm->data)[i*cols+j];
    }
  }
	
  free(myIm->data);

  myIm->data = newdata;//change pixel data field
  myIm->rows = r;//set new rows
  myIm->cols = c;//set new cols
	
  return 1;	
}
return 0;

}

/* This is an inversion image function. 
 * Each pixel's r, g, b is equal to the old value substracted from max color value. 
 */
int Inversion(Image* myIm){
    
    int colors = myIm->colors;//Store the max value of color first
    int read = didRead(myIm);
    if(read){
      printf("Inverting intensity...\n");
      int size = (myIm->rows) * (myIm->cols);
      for (int i = 0; i < size; i++){//Invert each r,g,b value in each pixel
      	myIm->data[i].r = colors - myIm->data[i].r;
      	myIm->data[i].g = colors - myIm->data[i].g;
      	myIm->data[i].b = colors - myIm->data[i].b;	
      }
      return 0;
    }
    return 0;

}

/* This is a swap function that swaps the value of green to red, blue to green,
 * and red to blue. This manipulation has to occur after the readImage function.
 * Since we allocate a temporary pixel pointer, we need to free it everything we use it.
 */
int Swap(Image* myIm){
    int read = didRead(myIm);
    if (read){
    printf("Swapping color channels...\n");
    int size = (myIm->rows) * (myIm->cols);//determine the size of pixel array
    for( int i = 0; i < size; i++){
	     Pixel *temp = malloc( sizeof(Pixel));//temporary pixel to store values in order for swapping
	     temp->r = myIm->data[i].r;
	     myIm->data[i].r = myIm->data[i].g ;
	     myIm->data[i].g = myIm->data[i].b;
	     myIm->data[i].b = temp->r ; 
  	 if (temp != NULL){
       free(temp);                                
      } 
    }
    }
    return 0;
  
}

void GrayScale(Image *myIm){
    int read = didRead(myIm);
    if(read){
	printf("Converting to grayscale...\n");
        int size = (myIm->rows) * (myIm->cols);
        for(int i=0;i<size;i++){
            int red = myIm->data[i].r;
            int green = myIm->data[i].g;
            int blue = myIm->data[i].b;
            int intensity = 0.30*red + 0.59*green + 0.11*blue;
            myIm->data[i].r = intensity;
            myIm->data[i].g = intensity;
            myIm->data[i].b = intensity;
        }
    }
}

void Contrast(Image *myIm, float cnamt){
    int read = didRead(myIm);
    if (read){
	printf("Adjusting contrast by %.2f...\n",cnamt);
        int size = (myIm->rows) * (myIm->cols);
        for(int i=0;i<size;i++){
          double red = Adjustment(myIm->data[i].r, cnamt);
          double green = Adjustment(myIm->data[i].g, cnamt);
          double blue = Adjustment(myIm->data[i].b, cnamt);
          myIm->data[i].r = Saturation(red);
          myIm->data[i].g = Saturation(green);
          myIm->data[i].b = Saturation(blue);
        }
    }
        
}

void Brighten(Image *myIm, int bramt){
    int read = didRead(myIm);
    if (read){
      printf("Adjusting brightness by %d...\n",bramt);
      int size = (myIm->rows) * (myIm->cols);
      for(int i=0; i<size; i++){
        myIm->data[i].r = Saturation((double)(myIm->data[i].r + bramt));
        myIm->data[i].b = Saturation((double)(myIm->data[i].b + bramt));
        myIm->data[i].g = Saturation((double)(myIm->data[i].g + bramt));
      }
    }
}

void Blur(Image *myIm, float sigma){
  int read = didRead(myIm);
  if(read){
    printf("Applying blur filter, sigma %.2f...\n",sigma);
    int width = ceil(sigma*10);
    if (width % 2 == 0){
      width ++;
    }
    int size = (myIm->rows) * (myIm->cols);
    Pixel *temp = malloc( sizeof(Pixel)*size);
    for(int i=0;i<size;i++){
        temp[i].r = (int)(Convolution(1, myIm, i, width, sigma));
        temp[i].g = (int)(Convolution(2, myIm, i, width, sigma));
        temp[i].b = (int)(Convolution(3, myIm, i, width, sigma));
    }
    
    free(myIm->data);
    myIm->data = temp;
  }
}


void Sharpen(Image *myIm, float sigma, float intensity){
  int read = didRead(myIm);
  if(read){
    printf("Applying sharpen filter, sigma %.2f, intensity %.2f...\n",sigma, intensity);
    int width = ceil(sigma*10);
    if (width % 2 == 0){
      width ++;
    }
    int size = (myIm->rows) * (myIm->cols);
    Pixel *temp = malloc( sizeof(Pixel)*size);
    for(int i=0;i<size;i++){
        temp[i].r = Saturation(myIm->data[i].r - (Convolution(1, myIm, i, width, sigma)- myIm->data[i].r) * intensity);
        temp[i].g = Saturation(myIm->data[i].g - (Convolution(2, myIm, i, width, sigma)- myIm->data[i].g) * intensity);
        temp[i].b = Saturation(myIm->data[i].b - (Convolution(3, myIm, i, width, sigma)- myIm->data[i].b) * intensity);
        //myIm->data[i] = temp[i];
    }
    
    free(myIm->data);
    myIm->data = temp;
  }

}

void SaltAndPepperNoise(Image *myIm){
  int read = didRead(myIm);
  if (read){
      printf("Applying Noise Effect...\n");
      int size = (myIm->rows) * (myIm->cols);
      int count = 5;
      for(int i=0;i<size;i=i+5+count){
          if(myIm->data[i].r > 128 && myIm->data[i].g > 128 &&  myIm->data[i].b > 128){ //light pixels.
              for(int x=0;x<2;x++){
              myIm->data[i+x].r = 96;
              myIm->data[i+x].g = 96;
              myIm->data[i+x].b = 96;
              }

          } 
          if (myIm->data[i].r < 128 && myIm->data[i].g < 128 &&  myIm->data[i].b < 128){
              myIm->data[i].r = 96;
              myIm->data[i].g = 96;
              myIm->data[i].b = 96;

          }
          count ++;

      } 
  }
}

double sq(double x){
  return x*x;
}

int Saturation(double color){
  int max = 255;
  int min = 0;
  if(color>max){
    color = max;
  } else if(color<min){
    color = min;
  } else {
    color = (int)(color);
  }

  return color;
}
double Adjustment(int color, float cnamt){
  color = color - 128;
  double cnColor = color * cnamt + 128;
  return cnColor;

}


double Convolution(int color, Image *myIm, int i, int width, float sigma){
 int rows = myIm->rows;
 int cols = myIm->cols;
 int r = i/cols;
 int c = i- cols*r;
 double sum = 0.0;
 double normalfac = 0.0;
 switch(color) {
  case 1:
    for (int x=r-(width/2); x<=(r+(width/2)); x++){
      for(int y=c-(width/2); y<=(c+(width/2)); y++){
        if(x>rows-1 || x < 0 || y>cols-1 || y<0 ){
        } else {
          int dx = abs(x-r);
          int dy = abs(y-c);
          double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
          normalfac += g;
	  sum += (myIm->data[x*cols+y].r * g);
        }
      }
    }
    break;
  case 2:
    for (int x=r-(width/2); x<=(r+(width/2)); x++){
      for(int y=c-(width/2); y<=(c+(width/2)); y++){
        if(x>rows-1 || x < 0 || y>cols-1 || y<0 ){
        } else {
          int dx = abs(x-r);
          int dy = abs(y-c);
          double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
          normalfac += g;
          sum += (myIm->data[x*cols+y].g * g);
        }
      }
    }
    break;
  case 3:
    for (int x=r-(width/2); x<=(r+(width/2)); x++){
      for(int y=c-(width/2); y<=(c+(width/2)); y++){
        if(x>rows-1 || x < 0 || y>cols-1 || y<0 ){
        } else {
          int dx = abs(x-r);
          int dy = abs(y-c);
          double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));
          normalfac += g;
          sum += (myIm->data[x*cols+y].b * g);
        }
      }
    }
    break;

  }
  return sum/normalfac;
 }



