/* a6.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: This is an image-manipulation program processing PPM image. Here is the main function.
 *
 */


//include header files
#include "../include/menuUtil.h"
#include "../include/ppmIO.h"
#include "../include/imageManip.h"


int main () {
    mainmenu();//display the menu and follow the instructions of the menu
    return 0;//exit when user finishes all operations
}

