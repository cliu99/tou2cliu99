/* imageManip.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 23, 2016
 *
 * Description: Functions used for manipulating and processing image.
 */

//include header files
#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"

/*Function to test if we have already read an image.
 * if we have, return 1; if we have not, print error message and return 0.*/
int didRead(Image *myIm){
	if (myIm->data == NULL) {//Check if we have read an image
		fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
		return 0;
	}
	return 1;
}
/*
*  This is the crop function which takes (x1,y1) as the upper left corner
*  and (x2,y2) as the lower right corner for the crop image.
*  In our program, we decided that x2 has to be bigger than/equal to x1, and 
*  same for y2. We did not like inverted crop and the 
*  assignment did not specify the rules. So we made our rules.
*  Both (x1,y1) and (x2,y2) has to be in the boundary of the original image.
*/
void Crop(Image* myIm, int x1, int y1, int x2, int y2) {
	if (x2<=x1 || y2<=y1) {//check is we have invalid input
		fprintf(stderr, "Invalid dimension input!\n");
		return;
	}
	//Check if we have read an image
	int read = didRead(myIm);
	//If we have already read an image, then crop the image
	if (read){
		//rows and cols for the cropped image.	
		int r = x2 - x1;
		int c = y2 - y1;
		int newsize = r * c;//new image size: r times c
		//rows and cols of the old image.
		int rows = myIm->rows;
		int cols = myIm->cols;

		int index = 0;
		//checks if the two corners are inside the image boundary.
		if ((x2>(rows-1)) || (y2>(cols-1))) {
			fprintf(stderr, "Dimensions indicated out of bound!\n");
			return;
		}

		printf("Cropping region from (%d, %d) to (%d, %d)...\n",x1,y1,x2,y2);

		Pixel* newdata = malloc( sizeof(Pixel) * newsize );//new data field to store pixels

		for (int i = x1; i<x2; i++) { //copy pixels to new pixel data field
			for (int j = y1; j<y2;j++) {
				newdata[index++] = (myIm->data)[i*cols+j];
			}
		}
	
		free(myIm->data);

		myIm->data = newdata;//change pixel data field
		myIm->rows = r;//set new rows
		myIm->cols = c;//set new cols	
	}
	return;
}

/* This is an inversion image function. 
 * Each pixel's r, g, b is equal to the old value substracted from max color value. 
 */
void Inversion(Image* myIm){
	int colors = myIm->colors;//Store the max value of color first
	int read = didRead(myIm);
	if(read){
		printf("Inverting intensity...\n");
		int size = (myIm->rows) * (myIm->cols);
		for (int i = 0; i < size; i++){//Invert each r,g,b value in each pixel
			myIm->data[i].r = colors - myIm->data[i].r;
			myIm->data[i].g = colors - myIm->data[i].g;
			myIm->data[i].b = colors - myIm->data[i].b;	
		}
	}
    return;
}

/* This is a swap function that swaps the value of green to red, blue to green,
 * and red to blue. This manipulation has to occur after the readImage function.
 * Since we allocate a temporary pixel pointer, we need to free it everything we use it.
 */
void Swap(Image* myIm){
	int read = didRead(myIm);
	if (read){
		printf("Swapping color channels...\n");
		int size = (myIm->rows) * (myIm->cols);//determine the size of pixel array
		for(int i = 0; i < size; i++){
			Pixel *temp = malloc( sizeof(Pixel));//temporary pixel to store values in order for swapping
			temp->r = myIm->data[i].r;//Store the red value
			myIm->data[i].r = myIm->data[i].g ;
			myIm->data[i].g = myIm->data[i].b;
			myIm->data[i].b = temp->r ; 
			if (temp != NULL){//free temp pointer
				free(temp);                                
			} 
		}
	}
	return;
}

/* This is a function to convert an image into grayscale image. This is done by setting each color value of a pixel tp a given amount (calculated by 0.3*red+0.59*green+0.11*blue)
 * This manipulation has to occur after the readImage function.
 */
void GrayScale(Image *myIm){
    int read = didRead(myIm);
    if(read){//Check if we have read an image
	printf("Converting to grayscale...\n");
        int size = (myIm->rows) * (myIm->cols);//number of pixels in the image
        for(int i=0;i<size;i++){//traverse each pixel and set r,g,b to "intensity" - a weighted sum of r,g,b
            int red = myIm->data[i].r;//old red value
            int green = myIm->data[i].g;//old green value
            int blue = myIm->data[i].b;//old blue value
            int intensity = 0.30*red + 0.59*green + 0.11*blue;//a calculated weighted sum of r,g,b
			/*Assign new r,g,b values to myIm*/
            myIm->data[i].r = intensity;
            myIm->data[i].g = intensity;
            myIm->data[i].b = intensity;
        }
    }
}

/*This is a function to adjust contrast of an image. 
 *It is done by converting unsigned colors into range [-128, 127] and multiply the value by a contrast factor, then plus 128 back. This manipulation is in another called "Adjustment". After adjustment, we call "Saturation" to make sure that the color value belongs to [0, 255].
 *This manipulation has to occur after the readImage function.
 */

void Contrast(Image *myIm, float cnamt){
    int read = didRead(myIm);
    if (read){//Check if we have read an image
	printf("Adjusting contrast by %.2f...\n",cnamt);
        int size = (myIm->rows) * (myIm->cols);//number of pixels in the image
		for(int i=0;i<size;i++){//traverse each pixel and adjust&saturate each value 
            /*Adjust each color value in each pixel by calling the adjustment funtion*/
			double red = Adjustment(myIm->data[i].r, cnamt);
			double green = Adjustment(myIm->data[i].g, cnamt);
			double blue = Adjustment(myIm->data[i].b, cnamt);
			myIm->data[i].r = Saturation(red);//Saturate red and assign it to myIm
			myIm->data[i].g = Saturation(green);//Saturate green and assign it to myIm
			myIm->data[i].b = Saturation(blue);//Saturate blue and asign it to myIm
        }
    }
        
}

/* This is a function to adjust the brightness of an image.
 * This is done by adding a given brightness amount to each r,g,b value.
 * After addition of value we saturate each value to make sure they belong to range [0, 255].
 * This manipulation has to occur after the readImage function.
 * */
void Brighten(Image *myIm, int bramt){
    int read = didRead(myIm);
    if (read){//Check if we have read an image
    printf("Adjusting brightness by %d...\n",bramt);
    int size = (myIm->rows) * (myIm->cols);
        for(int i=0; i<size; i++){//traverse each pixel and add brightness amount(can be negative) to each value value in every pixel
			/*Saturate new color values and assign them to myIm*/  
    		myIm->data[i].r = Saturation((double)(myIm->data[i].r + bramt));
    		myIm->data[i].b = Saturation((double)(myIm->data[i].b + bramt));
    		myIm->data[i].g = Saturation((double)(myIm->data[i].g + bramt));
		}
    }
}

/* This is a function to blur the image by a given sigma. 
 * This is done by looking at "a matrix of" values surrounding a pixel's value and set it to a special kind of average (calculated by function "Convolution").
 * This should also occur after the readImage function.
 * */
void Blur(Image *myIm, float sigma){
	int read = didRead(myIm);
	if(read){//check if we have read an image
    	printf("Applying blur filter, sigma %.2f...\n",sigma);
    int width = ceil(sigma*10);//the width of our square matrix
    if (width % 2 == 0){//make sure the width of the matrix is an odd number (since we will need a center pixel)
      width ++;
    }
    int size = (myIm->rows) * (myIm->cols);//number of pixels in the image
    Pixel *temp = malloc( sizeof(Pixel)*size);//new pixel array to hold blurred pixel data
    for(int i=0;i<size;i++){//traverse each pixel and put the calculated new value (by "Convolution") into our new pixel data field
        temp[i].r = (int)(Convolution(1, myIm, i, width, sigma));
        temp[i].g = (int)(Convolution(2, myIm, i, width, sigma));
        temp[i].b = (int)(Convolution(3, myIm, i, width, sigma));
    }
    
    free(myIm->data);//free the old pixel data field
    myIm->data = temp;//assign the new pixel data field to our image
  }
}

/* This is a function to sharpen the image by a given sigma and a given intensity. 
 * This is done by computing the difference between the blurred image and the original image and then subtract the value from the original image. Also, we multiply the difference by intensity to make the sharpening effect stronger/weaker. 
 * This should also occur after the readImage function.
 * */
void Sharpen(Image *myIm, float sigma, float intensity){
	int read = didRead(myIm);
	if(read){//check if we have read an image
		printf("Applying sharpen filter, sigma %.2f, intensity %.2f...\n",sigma, intensity);
		int width = ceil(sigma*10);//width(size) of our matrix
    	if (width % 2 == 0){//make sure the width is an odd number
    		width ++;
    	}
    	int size = (myIm->rows) * (myIm->cols);//number of pixels in our image
    	Pixel *temp = malloc( sizeof(Pixel)*size);//new pixel array to hold data
    	for(int i=0;i<size;i++){//traverse each pixel and compute the new sharpened value, put the new values into our new pixel data field
        	temp[i].r = Saturation(myIm->data[i].r - (Convolution(1, myIm, i, width, sigma)- myIm->data[i].r) * intensity);
        	temp[i].g = Saturation(myIm->data[i].g - (Convolution(2, myIm, i, width, sigma)- myIm->data[i].g) * intensity);
        	temp[i].b = Saturation(myIm->data[i].b - (Convolution(3, myIm, i, width, sigma)- myIm->data[i].b) * intensity);
    	}
		free(myIm->data);//free old pixel data field
		myIm->data = temp;//assign new pixel data field to our image
  	}
}

/*Extra Credit Functions*/

/* This is a function to rotate the image by 90 degress, clockwise. 
 * This is done by creating a new pixel array, and put corresponding value of the original image into the new pixel array.
 * This should also occur after the readImage function.
*/
void Rotate(Image *myIm){
	int read = didRead(myIm);
	if(read){//check if we have read an image
		printf("Rotating the image by 90 degrees clockwise...\n");
		int cols = myIm->cols;
		int rows = myIm->rows;
		Pixel *temp = malloc ( sizeof(Pixel) * rows * cols);//create a new pixel array to hold data
		for(int i=0;i<cols;i++){//traverse each pixel in the new pixel array, copy value from the original pixel array in the original image
        	for(int j=0;j<rows;j++){
				/*copy value from the original image; i and j are rows/cols for the new rotated image*/
        		temp[i*rows+j].r= myIm->data[(rows-j-1)*cols+i].r;
        		temp[i*rows+j].g= myIm->data[(rows-j-1)*cols+i].g;
        		temp[i*rows+j].b= myIm->data[(rows-j-1)*cols+i].b;
        	}
    	}
    	free(myIm->data);//free the old pixel data field
    	myIm->data = temp;//assign the new pixel data field to our image
    	myIm->rows = cols;//set new columns
    	myIm->cols = rows;//set new rows
	}
}

/* This is a function to apply rainbow stripe effect to an image.
 * This is done by seperating an image into 7 parts, and change the r/g/b values in each part by a given formula to create red, orange, yellow stripes, etc. 
 * This should also occur after the readImage function.
 * */
void Stripe(Image *myIm){
    int read = didRead(myIm);
    if(read) {//check if we have read an image
	printf("Applying rainbow stripe filter...\n");
    int rows = myIm->rows;
    int width = rows/7;//the width of each stripe
    int cols = myIm->cols;//the number of columns in our image
    int size = rows*cols;//number of pixels in our image
    int i = 0;
		while (i < size) {
       		if(i<width*cols){//the first stripe - change g/b values to make red stripe      
            	myIm->data[i].g = Saturation((myIm->data[i].g * 0.12));
            	myIm->data[i].b = Saturation((myIm->data[i].b * 0.12));
        	} else if (i<width*cols*2){//the second stripe - change b/g values to make orange stripe
            	myIm->data[i].b = Saturation((myIm->data[i].b * 0.12));
            	myIm->data[i].g = Saturation((myIm->data[i].g * 0.5));
        	} else if (i<width*cols*3){//the third stripe - change b values to make yellow stripe
            	myIm->data[i].b = Saturation((myIm->data[i].b * 0.12));
        	} else if (i<width*cols*4) {//the fourth stripe - change r/g values to make green stripe 
            	myIm->data[i].r = Saturation((myIm->data[i].r * 0.12));
            	myIm->data[i].b = Saturation((myIm->data[i].b * 0.12));
     		} else if (i<width*cols*5) {//the fifth stripe - change r value to make cyan stripe
	        	myIm->data[i].r = Saturation((myIm->data[i].r * 0.12));
     		} else if (i<width*cols*6){//the six stripe - change g/r value to make blue stripe
		    	myIm->data[i].g = Saturation((myIm->data[i].g * 0.5));
            	myIm->data[i].r = Saturation((myIm->data[i].r * 0.12));
		    } else {//the last stripe - change r/g value to make purple stripe
				myIm->data[i].r = Saturation((myIm->data[i].r * 0.5));
		    	myIm->data[i].g = Saturation((myIm->data[i].g * 0.12));
			}
			i++;
		}
	}
}

//math helper function to get the square of a double
double sq(double x){
	return x*x;
}

//Saturation function to make sure a value is an integer between 0 and 255
int Saturation(double color){
	int max = 255;
	int min = 0;
	if(color>max){//if color > 255, then make it 255
		color = max;
	} else if(color<min){//if color < 0, then make it 0
		color = min;
	} else {//if the color value is okay, then cast it to an integer
		color = (int)(color);
	}
	return color;
}

//Adjustment function used for "Constrast" function to increase/decrease contrast by subtracting 128 from the value, multiply the result by contrast amount and then add 128 back
double Adjustment(int color, float cnamt){
	color = color - 128;
	double cnColor = color * cnamt + 128;//new adjusted color value
	return cnColor;
}


double Convolution(int color, Image *myIm, int i, int width, float sigma){
	int rows = myIm->rows;//total rows of image
	int cols = myIm->cols;//total columns of image
	int r = i/cols;//center pixel rows
	int c = i- cols*r;//center pixel columns
	double sum = 0.0;//sum is the pixel value calculated and will be returned after normalization
	double normalfac = 0.0;//if the pixel is near the boundary, we need to normalize the pixel value to avoid darkening/brightening it. Normalfac stores the sum of all g. If g is not equal to 1, then we divide the calculated pixel value by normalfac to normalize it.
  
/*visit each pixel in the matrix and calculate the sum of (g-value times pixel value) to get the blurred value*/
	for (int x=r-(width/2); x<=(r+(width/2)); x++) {
		for(int y=c-(width/2); y<=(c+(width/2)); y++){
        	if(x<=(rows-1) && x>=0 && y<=(cols-1) && y>=0) {//check if the pixel is in boundary
          		int dx = abs(x-r);//the offset from the center to the pixel
          		int dy = abs(y-c);//the offset from the center to the pixel
          		double g = (1.0 / (2.0 * PI * sq(sigma))) * exp( -(sq(dx) + sq(dy)) / (2 * sq(sigma)));//formula for computing g value
          		normalfac += g;
	  			if (color == 1) {//if color is 1, then we compute the values of red by adding the weighted red value
	  				sum += (myIm->data[x*cols+y].r * g);
	  			} else if (color == 2) {//if color is 2, then green value computed
	  				sum += (myIm->data[x*cols+y].g * g);
	  			} else {//if color is 3, then blue value computed
	  				sum += (myIm->data[x*cols+y].b * g);
	  			}
        	}
      	}
	}
 	return sum/normalfac;//return normalized value
}


