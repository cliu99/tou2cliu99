#ifndef TEXTFILE_H
#define TEXTFILE_H

#include <string>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <tuple>
#include <cmath>
#include <iostream>
#include <algorithm> 

class TextFile
{
public:
    TextFile(const std::string& filename="");
    std::vector<std::vector<std::string>> getNgram(unsigned n);
    bool OpenFile(const std::string& filename);

private:

    std::stringstream text_;
};

#endif 