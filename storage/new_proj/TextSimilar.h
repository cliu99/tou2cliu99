#ifndef TEXTSIMILAR_H
#define TEXTSIMILAR_H
#include "TextFile.h"
#include <vector>
#include <map>
#include <tuple>
#include <cmath>
#include <iostream>
#include <string>
#include <algorithm> 
#define threshold_l 16
#define threshold_m 12
#define threshold_h 12
#define n_l 5
#define n_m 5
#define n_h 3

class TextSimilar
{
public:
    TextSimilar();
    std::vector<std::pair<std::size_t,std::size_t>> GetSimilar(const     std::vector<std::string>& filename,char mode='m');

private:
    void InitData(const std::vector<std::string> &filename, char mode);
    unsigned ComputeSimilar(std::size_t x,std::size_t y);
    std::map<std::vector<std::string>,std::vector<unsigned> >dict_;
};

#endif // TEXTSIMILAR_H
