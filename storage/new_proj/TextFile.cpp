#include "TextFile.h"

TextFile::TextFile(const std::string& filename)
{
    if(filename!="")
        OpenFile(filename);
}

bool TextFile::OpenFile(const std::string& filename)
{
    std::ifstream fin;
    fin.open(filename);
    if(!fin.is_open())
        return false;
    std::string temp;
    while(!fin.eof())
    {
        char c=fin.get();
        if(c>='A' && c<='Z')
            c=c-'A'+'a';

        if(c>='a' && c<='z')
            temp+=c;
        else if(c=='\n')
            temp+='\n';
        else if(temp.size()>0 && temp[temp.size()-1]!=' ')
            temp+=' ';
    }
    text_.str(temp);
    return true;
}

std::vector<std::vector<std::string>> TextFile::getNgram(unsigned n){
    std::vector<std::vector<std::string> > NgramCollection;
    std::string word;
    std::vector<std::string> ngram;
    
    for (int i = 0; i<n; i++) {//read n words in the file
    text_ >> word;
    ngram.push_back(word);
    }
    
    NgramCollection.push_back(ngram);//add 1st ngram
    
    text_ >> word;//(n+1)th word
    
  while (!text_.eof()) {
        ngram.push_back(word);
        ngram.erase(ngram.begin());
        NgramCollection.push_back(ngram);
        text_ >> word;//(n+1)th word
     //   std::cout << word << " ";
    }
    
    return NgramCollection;
   
}
    

