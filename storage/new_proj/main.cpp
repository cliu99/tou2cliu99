#include "TextSimilar.h"

int main(int argc,char** argv)
{
    std::ifstream fin;
    std::vector<std::string> name;
    std::string listfilename;
    std::string mode;
    if(argc<=1)
    {
        std::cout<<"error argument"<<std::endl;
        return 0;
    }
    else if (argc == 2)
    {
        mode="m";
        listfilename=argv[1];
    }
    else if(argc==3)
    {
        mode=argv[2];
        listfilename=argv[1];
    }

    fin.open(listfilename);
    if(!fin.is_open())
    {
        std::cerr<<"Error: file not found"<<'\n';
        return 0;
    }

    while(!fin.eof() && !fin.fail())
    {
        std::string temp;
        fin>>temp;
        name.push_back(temp);
    }

    TextSimilar comp;
    auto simIndex = comp.GetSimilar(name,mode[0]);
    std::cout<<"Detected Suspicous Pairs:"<<'\n';
    for(const std::pair<std::size_t,std::size_t>& simPair : simIndex)
        std::cout<<name[simPair.first]<<" AND "<<name[simPair.second]<<'\n';
     return 0;   
}
