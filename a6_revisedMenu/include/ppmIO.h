/* ppmIO.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for reading and writing PPM image functions.
 *
 */

//include guard

#ifndef PPMIO_H
#define PPMIO_H

//Include necessary C library
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

//Struct definition for Pixel and Image here

//Pixel has three values, r=red, g=green, b=blue
typedef struct _pixel {
	unsigned char r;
	unsigned char g;
	unsigned char b;
} Pixel;

//An image consists of a Pixel array and three parameters: the height and width of the image, and max value of colors
typedef struct _image {
	Pixel *data;
	int rows;
	int cols;
	int colors;
} Image;

//function prototypes go here

//Function to read an input PPM image
int readimage(Image* myIm, char *filename);

//Function to write an output PPM image
int writeimage(Image* myIm, char* filename);

#endif 
