/* imageManip.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Functions used for manipulating and processing image.
 */


#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"

/*
*  This is the crop function which takes (x1,y1) as the upper left corner
*  and (x2,y2) as the lower right corner for the crop image.
*  In our program, we decided that x2 has to be bigger than/equal to x1, and 
*  same for y2. We did not like inverted crop and the 
*  assignment did not specify the rules. So we made our rules.
*  Both (x1,y1) and (x2,y2) has to be in the boundary of the original image.
*/
int Crop(Image* myIm, int x1, int y1, int x2, int y2) {
    if (x2<=x1 || y2<=y1) {//check is we have invalid input
	fprintf(stderr, "Invalid dimension input!\n");
	return 0;
	}
    if (myIm->data == NULL) { //check if we have read an image
	fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
	return 0;
	}
  //rows and cols for the cropped image.	
  int r = x2 - x1;
  int c = y2 - y1;

  int newsize = r * c;
  int rows = myIm->rows;
  int cols = myIm->cols;

  int index = 0;
  //checks if the two corners are inside the image boundary.
  if ((x2>(rows-1)) || (y2>(cols-1))) {
  fprintf(stderr, "Dimensions indicated out of bound!\n");
  return 0;
  }

  printf("Cropping region from (%d, %d) to (%d, %d)...\n",x1,y1,x2,y2);

  Pixel* newdata = malloc( sizeof(Pixel) * newsize );//new data field to store pixels

  for (int i = x1; i<x2; i++) { //copy pixels to new pixel data field
    for (int j = y1; j<y2;j++) {
      newdata[index++] = (myIm->data)[i*cols+j];
    }
  }
	
  free(myIm->data);

  myIm->data = newdata;//change pixel data field
  myIm->rows = r;//set new rows
  myIm->cols = c;//set new cols
	
return 1;	

}

/* This is an inversion image function. 
 * Each pixel's r, g, b is equal to the old value substracted from max color value. 
 */
int Inversion(Image* myIm){
    if (myIm->data == NULL) {//Check if we have read an image
	  fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
	  return 0;
	}
    int colors = myIm->colors;//Store the max value of color first

    printf("Inverting intensity...\n");
    int size = (myIm->rows) * (myIm->cols);
    for (int i = 0; i < size; i++){//Invert each r,g,b value in each pixel
	myIm->data[i].r = colors - myIm->data[i].r;
	myIm->data[i].g = colors - myIm->data[i].g;
	myIm->data[i].b = colors - myIm->data[i].b;	
    }
    return 0;

}

/* This is a swap function that swaps the value of green to red, blue to green,
 * and red to blue. This manipulation has to occur after the readImage function.
 * Since we allocate a temporary pixel pointer, we need to free it everything we use it.
 */
int Swap(Image* myIm){
    if (myIm->data == NULL) {//check if we have read an image
	fprintf(stderr, "Wait! You have not read an image. Please read an image first!\n");
	return 0;
    }
    printf("Swapping color channels...\n");
    int size = (myIm->rows) * (myIm->cols);//determine the size of pixel array
    for( int i = 0; i < size; i++){
	Pixel *temp = malloc( sizeof(Pixel));//temporary pixel to store values in order for swapping
	temp->r = myIm->data[i].r;
	myIm->data[i].r = myIm->data[i].g ;
	myIm->data[i].g = myIm->data[i].b;
	myIm->data[i].b = temp->r ; 
	if (temp != NULL)  free(temp);                                
     } 
    return 0;
}

//Functions below are yet to be written in phase 2.
void Contrast() {
    printf("Sorry, the Contrast function has not been written yet.\n");
}

void Blur(){
    printf("Sorry, the Gaussian blur function has not been written yet.\n");
}

void Brighten(){
    printf("Sorry, the brighten function has not been written yet.\n");
}

void GrayScale(){
    printf("Sorry, the grayscale function has not been written yet.\n");
    }

void Sharpen(){
    printf("Sorry, the sharpen function has not been written yet.\n");
}


