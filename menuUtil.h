/* menuUtil.h
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 16, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Headers for printing menu and reading user input functions.
 *
 */

//include guard

#ifndef MENUUTIL_H
#define MENUUTIL_H

//include necessary C library and header files

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include "ppmIO.h"

//function prototypes go here

//This function will print Goodbye and free memory when the user accidentally hit EOF in stdin.
void printGoodbye(char* buffer, Image *myIm);

/* If we read a "r" or "w", then call this function to first check
 * if the input is valid. If it is valid, then this function will
 * call the corresponding functions for read/write an image in 
 * imageManip.c
 */
int readOrWrite(char ch, Image *myIm, char* buffer, int* length);


/* If the user enters 'c', this function first checks if the 
 * user has entered a valid input for either crop or contrast.
 * If the input is invalid, this function print out an error 
 * message and prompt the menu list again for the user. If the 
 * input is valid, this function then calls the crop/contrast function
 * in imageManip.c to execute.
 */
int cropOrContrast(char ch, Image *myIm, char* buffer);

/* This function is called when the user enters 'g', or 'i', or 'o'. or 't'.
 * It first check if the input is valid. If the input is invalid, it prints out
 * an error message and prompt the user again. If the input is valid, it calls 
 * the corresponding function in imageManip.c (grayscale, inversion, rotation, or rainbow).
 */
int grayOrInvertOrRotateOrStripe(char ch, Image *myIm, char* buffer);

/* This function is called when the user enters 's'. 
 * It first checks if the input is valid and if the user meant
 * sharpen or swap channels. If the input is invalid, it prints
 * out an error message and prompt the user again. If the input is valid,
 * it calls the corresponding functions in imageManip.c to execute.
 */
int sharpenOrSwap(char ch, Image *myIm, char* buffer);

/* This fucntion is called when the user enters 'b'.
 * It will first check if the input is valid or not.
 * If invalid, it will print an error message and prompt again.
 * If valid, it will call the corresponding functions in imageManip.c,
 */
int blurOrBrighten(char ch, Image *myIm, char* buffer);


//Function to read user input and call other PPM image processing functions
int mainmenu();

//Function to print a menu
int printmenu();

#endif 


