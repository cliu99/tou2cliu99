/* menuUtil.c
 *
 * Author:
 * Tingting Ou JHED: tou2
 * Chuqiao Liu JHED: cliu99
 *
 * Written on Oct. 14, 2016
 * Last modified on Oct. 16, 2016
 *
 * Description: Functions used to print the menu and read user input
 *
 */

#include "../include/imageManip.h"
#include "../include/ppmIO.h"
#include "../include/menuUtil.h"


//Print menu function
int printmenu(){
	printf("Main menu:\n\tr <filename> - read image from <filename>\n\tw <filename> - write image to <filename>\n\tc <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n\ti - invert intensities\n\ts - swap color chanels\n\tg - convert to grayscale\n\tbl <sigma> - Gaussian blur with the given radius (sigma)\n\tsh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n\tbr <amt> - change brightness (up or down) by the given amount\n\tcn <amt> - change contrast (up or down) by the given amount\n\tq - quit\n");
	printf("Enter choice: ");	
	return 0;
}


//The main menu funtion to read input and call correspondent functions
int mainmenu() {
	printmenu();
	int length = 50;//MAX buffer size in the first place
	char* buffer = malloc(sizeof(char) * length);//buffer word to store file name string
	Image myIm;//My image
	myIm.data = NULL;//initialize the pixel field pointer to NULL
	char ch; //user input.

	while((ch = getchar())!=EOF && ch != 'q'){
		 if (ch == 'r' || ch == 'w') { //if we got r or w, call function to read the input and call functions
		 	int check = readOrWrite(ch, &myIm, buffer, &length);
		 	if (check == -1){ //we don't need to free buffer and create a new one if check is -1
		 		continue;
		 	} 
		 	char *empbuffer = malloc(sizeof(char)*length);//after using the buffer we free it and create a new one
			if (buffer!=NULL){
				free(buffer); 
			} 
			buffer = empbuffer;
		 	
		 } else if (ch == 'c') { //now if the user enters c/cn
			cropOrContrast(ch, &myIm, buffer);
		 } else if (ch == 'g' || ch == 'i'){
			grayOrInvert(ch, &myIm, buffer);
		 } else if ( ch == 's'){
			sharpenOrSwap(ch, &myIm, buffer);
		 } else if (ch == 'b'){ 
			blurOrBrighten(ch, &myIm, buffer);
		 } else { //if the user has not entered anything like those on the menu.
			  	if (!isspace(ch)) {//if we got a character that's not on the menu, print error message
			    	fprintf(stderr, "No such Command! Enter again!\n");
			    	while((ch=getchar())!='\n');//read the whole line then
			  	} else{//if we got some spaces, continue to get characters
			  	 	continue;
			  	}
		 }
    }    
    //if the user has entered q or reaches EOF.
	printf("Goodbye!\n");
	if (buffer!= NULL) free(buffer);//free the memory we have used
	free(myIm.data);
	return 0;
}



//Function to print Goodbye and free memory when accidentally hit EOF
void printGoodbye(char* buffer, Image *myIm){
	printf("Goodbye!\n");
	free(buffer);
	free(myIm->data);
}

/*Functions to read user input and to call correspondent functions according to the characters we have read*/
/*Classied by the first character we get in a line*/


//If we read a "r" or "w", then call this function to read the input
int readOrWrite(char ch, Image *myIm, char* buffer, int* length){
	int rw = 0; //this is a flag to tell if the input is readImage or writeImage.
	int invalid = 0;//used for checking invalid input
	int i = 0;//index used to store buffer characters

	if (ch == 'r'){
		rw = 1;
	}
	if (ch == 'w'){
		rw = -1;
	}

	ch = getchar();//get the next character to start checking

	//if a user types r/w and adds characters following r/w, it is invalid.
	if (!(isspace(ch))) {
		invalid = 1;
		while((ch=getchar())!='\n');
	}
		
	//a user may type multiple spaces before the next char.
	while(isspace(ch)){
	//still invalid if the user type r/w and a bunch of spaces and hits enter.
		if (ch == '\n'){
			invalid = 1;
			break;
		}
		ch = getchar();
	}
	//If invalid input, then return -1 to show that we have not used the buffer
	if (invalid == 1){
		fprintf(stderr, "Invalid input!\n"); 
		printmenu();
		return -1;
	}
	//move the cursor back a char to get the first character of the filename.    
	ungetc(ch, stdin);
 	while (!isspace(ch=getchar())) {
		//if the input reaches EOF. Exit.
            	if (ch == EOF){
			printGoodbye(buffer, myIm);
			return 0;
		}
		//if the input is too large. Reallocate the memory for the buffer string.	
		if (i == (*length)-2) {
			*length = (*length) * 2;
			buffer = realloc(buffer, sizeof(char) * (*length));
			buffer[i++] = ch;
		} else {
			buffer[i++] = ch;
		  }	
	}
	buffer[i]='\0';//Terminate our buffer word

	//the user should not enter any chars afterwards (aka only spaces and \n follow.)
	int invalidcheck = 0;
	if (ch != '\n') {//if there are inputs after the filename before a newline
		while ((ch = getchar())!='\n') {//read the whole line
			if (!isspace(ch)) {
	  		invalidcheck = 1;
		}
	}
	//if the user did input something after the filename, it is invalid.
	//discard the content of the buffer.
		if (invalidcheck == 1) {
			fprintf(stderr, "Invalid input!\n"); 
			printmenu();
			return 0;
		}
	}

	if (rw == 1){//if we have a read flag and valid input filename, read the image
		readimage(myIm, buffer);
	}
	if (rw == -1){//if we have a write flag and valid output filename, write the image
	  	writeimage(myIm, buffer); 
	}
	printmenu();
	return 0;
}

int cropOrContrast(char ch, Image *myIm, char* buffer){
		//Get the next character. Check if we hit EOF
		if((ch = getchar())==EOF) {
		    printGoodbye(buffer, myIm);
			return 0;
		}
		//If we got 'n' we do operations to prepare for constrast function
		if (ch == 'n') {
		  float cnamt;
		  int scan = scanf("%f",&cnamt);
		  //if scanf gets EOF.
		  if (scan == -1) {
			printGoodbye(buffer, myIm);
			return 0;
		  } else if (scan == 0) { 
		    //if scanf did not get a float for amount.
		        while((ch=getchar())!='\n');//read the whole line first
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return -1; 
		  } else {
		        ch = getchar();
		        int invalid = 0;//new input check variable defined for following checks
			
			//check if the inputs after cn has chars or EOF.
			while(ch!= '\n') {//read until the newline character
				if (ch == EOF) {
					printGoodbye(buffer, myIm);
					return 0;
				}
				if (!isspace(ch)) {
					invalid = 1;
				}
				ch = getchar();
			}
			if (!invalid) {//If we pass each test, then we call Contrast functions
				Contrast();
			}
			else { 
			    fprintf(stderr, "Invalid input!\n");
			}
		  printmenu();
		  return 0;
		  }
		} else if (ch == '\n') {//if we only get a c, then the input is invalid 
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return -1;
		} else if (!isspace(ch)) { //anything other than 'c '(e.g cb, ca) should be invalid.
		  	fprintf(stderr, "Invalid input!\n");
		        while(((ch=getchar())!= '\n')) {//then we read the whole line
		    	  if(ch==EOF) {
			      printGoodbye(buffer, myIm);
			      return 0;
		   	    }
			}
		  	printmenu();
		  	return -1;
		} else {//if have have a space after 'c' we prepare to do operations for cropping
		  int array[4];//used to store numbers scanned
	          int scans = scanf("%d %d %d %d", &array[0], &array[1],&array[2], &array[3]);
		  if (scans != 4) {//if our scans fail
		  	if (scans == -1){//if we hit EOF
			  	printGoodbye(buffer, myIm);
				return 0;
		  	}
			//if the input is invalid but not a EOF, we read the whole line
		  	fprintf(stderr, "Invalid input!\n");
		    
		  	while(((ch=getchar())!= '\n')) {
		       		if (ch == EOF) {
					printGoodbye(buffer, myIm);
					return 0;
		      		}
		      	}
			printmenu();
			return -1;
		  }
		  //if scanf successfully get 4 integers. 
		  //Read till \n to check if the user has entered additional invalid inputs.
		  ch = getchar();
		  int invalid = 0;//new variable for checking invalid input after 4 integers
		  while(ch!= '\n') {
			    if (ch == EOF) {
				printGoodbye(buffer, myIm);
				return 0;
			    }
			    if (!isspace(ch)) {
			    	invalid = 1;
			    }
			ch = getchar();
		  }

		  if (!invalid) {//if the input is okay, crop the image
            		Crop(myIm,array[0], array[1], array[2], array[3]);
		  } else { //if input is invalid, print an error message
		    	fprintf(stderr, "Invalid input!\n");
		  }
		printmenu();
		return 0;
		}
}

int grayOrInvert(char ch, Image *myIm, char* buffer){
		//again command is another flag for g/i.
		int command = 0;//0 means invert, 1 means grayscale
		int invalid = 0;
		if (ch == 'g'){
			command = 1;
		}
		//no extra chars should appear after g/i till \n.
		while((ch=getchar())!='\n'){
			if (ch == EOF) {
			    printGoodbye(buffer, myIm);
			    return 0;
			}
			if(!isspace(ch)){
				invalid = 1;
				printf("Invalid input!\n");
			}
		}
		if(!invalid){
			if (command){//if command is 1, then it means we do grayscale
				 GrayScale();//yet to be written.
			} else {
				 Inversion(myIm);
			}
			printmenu();
			return 0;
		} else {//if invalid, do nothing and print menu, go back to get the next character
		    printmenu();
		    return 0;
		}
}

//Only Swap function has plentiful of comments

int sharpenOrSwap(char ch, Image *myIm, char* buffer){
	ch = getchar();
	int invalid = 0;
		if (ch == 'h' || isspace(ch)){
			if(ch == 'h'){ //if the user wants sh.
				if (isspace(ch=getchar())){
					float shsigma;
					float shamt ;  
					int scan = scanf("%f %f", &shsigma, &shamt);
					if (scan == 2){
						Sharpen();
						printmenu();
						return 0;
					} else {
						while((ch=getchar())!='\n'){
							if (ch == EOF){
								printGoodbye(buffer, myIm);
								return 0;
							}
						}
						printf("Invalid input (not enough arguments for Sharpen).\n");
						printmenu();
						return 0;
					}
				} else {
					printf("Invalid input!(too many chars for a single command)\n");
					printmenu();
					return 0;
				}
			} else {// if the user wants 's' we may need the swap function, prepare to check and call it
				while(ch!='\n'){//read the whole line to check if there is invalid input
					if(!isspace(ch)){//if we detect invalid input after s
						invalid = 1;
						printf("Invalid input!\n");
						while((ch = getchar())!='\n'){//read the whole line and go back to menu
							if (ch == EOF){
								printGoodbye(buffer, myIm);
								return 0;
							}
						}
						break;
					}
					ch = getchar();
				}

				if(!invalid){//if the input is valid, we swap the image
					Swap(myIm);
				}
					invalid = 0;
					printmenu();//print menu and go back to get the next character
					return 0;
			} 
		} else { //if the user types an invalid input starts with 's'.
		    while ((ch=getchar())!='\n'){//read the whole line
		      if(ch == EOF){
					printGoodbye(buffer, myIm);
					return 0;
		      }
		    }
			printf("Invalid inputs start with 's'\n");
			printmenu();
			return 0;
		}
}


int blurOrBrighten(char ch, Image *myIm, char* buffer) {
    ch = getchar();
    int flag = 0;
    if (ch == 'r' || ch == 'l'){
		if (ch == 'r')
		 flag = 1;  
		if (ch == 'l')
		 flag = -1; 

		ch = getchar();//get the third character in the line -- if input is valid it should be a non-newine space

		if (isspace(ch) && (ch != '\n')){//if the 3nd character is fine
		    int bramt;
		    float blamt;  
		    int scan;
		    if (flag == 1)
		   	 scan = scanf("%d", &bramt);
	            else
		    	scan = scanf("%f", &blamt);

		    if (scan == -1) {
			printGoodbye(buffer, myIm);		
			return 0;
		    }

		    int invalid = 0;

		    while((ch=getchar())!='\n'){// read the whole line to check if there is input after the parameter
			if (ch == EOF) {
			printGoodbye(buffer, myIm);		
			return 0;
			}
    
			if (!(isspace(ch))){
		    		invalid = 1;
		    	}
		    }

		    if (scan == 1 && invalid == 0){
		        if (flag == 1)
				Brighten();
			if (flag == -1)
				Blur();
		        printmenu();
		        return 0;
		    } else { //if our scan fails or there is input after the scanned number
			fprintf(stderr, "Invalid input! Please check the format of parameters.\n");
			printmenu();
			return 0;
		    }
		} else {//if the 3rd character is a newline (void of parameter) or if it is a non-space invalid character
			if (!isspace(ch))
			        while((ch=getchar())!='\n');
			fprintf(stderr, "Invalid input!\n");
			printmenu();
			return 0;
		}

    } else {//if the first character is neither r nor l, read the whole line and print erroe message
	if (ch != '\n')	{
	while((ch=getchar())!='\n');
	}
	fprintf(stderr, "Invalid input! Please specify function you need and add parameters..\n");
	printmenu();
	return 0;
    }
}


